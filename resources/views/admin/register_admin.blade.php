@extends('layouts.dashboard')
@section('page_heading','Register Admin')

@section('section')

<div class="col-sm-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                Please Correct the following errors.
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
         @endif
<div class="row">
    {{ Form::open(['url'=>'create_admin','id'=>'signup','role'=>'form']) }}
    <div class="col-lg-6">
        
        
            <div class="form-group">
                <label>Full Name</label>
                <input name="txtFullName" is="txtFullName" class="form-control" placeholder="Enter full Name" value="{{old('txtFullName')}}" />           
                @if ($errors->has('txtFullName'))
                    <div class="text-danger">{{ $errors->first('txtFullName', ':message'); }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input name="txtEmail" id="txtEmail" class="form-control" placeholder="Enter valid email" value="{{old('txtEmail')}}" />
                @if ($errors->has('txtEmail'))
                    <div class="text-danger">{{ $errors->first('txtEmail', ':message'); }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Password</label>
                <input name="txtPassword" id="txtPassword" class="form-control" type="password" placeholder="Combination of Alphabet and Numeric only" value="{{old('txtPassword')}}" />
                @if ($errors->has('txtPassword'))
                    <div class="text-danger">{{ $errors->first('txtPassword', ':message'); }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input name="txtCPassword" id="txtCPassword" class="form-control" placeholder="Combination of Alphabet and Numeric only"  type="password" value="{{old('txtCPassword')}}" />
                @if ($errors->has('txtCPassword'))
                    <div class="text-danger">{{ $errors->first('txtCPassword', ':message'); }}</div>
                @endif
            </div>
    </div>    
    <div class="col-lg-6">
        <div class="form-group">
            <label>Select Admin Rights</label>
            @foreach($roles as $role)
                <div class="radio">
                    <label>
                        <input type="radio" name="optionsRadios" id="radio{{ $role -> id }}" value="{{$role -> id}}" checked> {{$role -> name}}
                    </label>
                </div>
            @endforeach            
        </div>
    </div>
    <div class="col-lg-12">
        &nbsp;
    </div>
    <div class="col-lg-12">
        <button type="submit" class="btn btn-success">Submit</button>
        <input type="reset" class="btn btn-danger" value="Clear Form" />
    </div>
    {{ Form::close() }}
</div>
    <div class="row">
        &nbsp;
    </div>
</div>
@stop