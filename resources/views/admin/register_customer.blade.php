@extends('layouts.dashboard')
@section('page_heading','Register Prospect')

@section('section')
<link rel="stylesheet" href="assets/scripts/auto_complete/jquery-ui.css" type="text/css" />
{{ Html::script('assets/scripts/auto_complete/jquery.min.js'); }}         
{{ Html::script('assets/scripts/auto_complete/jquery-ui.min.js'); }}
<script>
        // Put jQuery 1.10.2 into noConflict mode.
    var $jq1 = jQuery.noConflict(true);
</script>
{{ Html::script('assets/scripts/register_customer.js'); }}
<div class="col-sm-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                Please Correct the following errors.
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
         @endif
         
{{ Form::open(['url'=>'create_customer','id'=>'signup','role'=>'form']) }}
<div class="row">    
    <div class="col-lg-10">
            <div class="form-group">
                <label>Full Name *</label>
                <input name="txtFullName" is="txtFullName" class="form-control" placeholder="Enter full Name" value="{{old('txtFullName')}}" />           
                @if ($errors->has('txtFullName'))
                    <div class="text-danger">{{ $errors->first('txtFullName', ':message'); }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Phone Number *</label><br />
                <select name="slPrefix" id="slPrefix" class="form-control" style="width: 20%; display: inline">
                    <option value="010">010</option>
                    <option value="011">011</option>
                    <option value="012">012</option>
                    <option value="013">013</option>
                    <option value="015">015</option>
                    <option value="015">016</option>
                    <option value="015">017</option>
                    <option value="015">018</option>
                    <option value="015">019</option>
                </select>
                <input name="txtPhoneNumber" id="txtPhoneNumber" class="form-control" style="width: 79%; display: inline" placeholder="E.g. 191337899" value="{{old('txtPhoneNumber')}}" />
                @if ($errors->has('txtPhoneNumber'))
                    <div class="text-danger">{{ $errors->first('txtPhoneNumber', ':message'); }}</div>
                @endif
            </div>
        <div class="form-group">
            <label>Address</label>
            <input name="txtAddress" id="txtAddress" class="form-control" placeholder="Enter address" value="{{old('txtAddress')}}" />
            <div class="text-danger" id="divAddress"></div>
        </div>
            <div class="form-group">
                <label>Agent *</label>
                <input name="txtAgent" id="txtAgent" class="form-control" placeholder="Part of Agent Name / AIA ID" value="{{old('txtAgent')}}" />           
                @if ($errors->has('txtAgent'))
                    <div class="text-danger">{{ $errors->first('txtAgent', ':message'); }}</div>
                @endif
            </div>
    </div>
</div><div class="row"> &nbsp;</div>
<div class="row">     
    <div class="col-lg-12">
        <button type="submit" class="btn btn-success">Submit</button>
        <input type="reset" class="btn btn-danger" value="Clear Form" />
    </div>
</div>
{{ Form::close() }}
    <div class="row">
        &nbsp;
    </div>
</div>
@stop