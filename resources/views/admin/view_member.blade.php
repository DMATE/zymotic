@extends('layouts.dashboard')
@section('page_heading','Search Agents')

@section('section')
<link rel="stylesheet" href="assets/scripts/auto_complete/jquery-ui.css" type="text/css" />
{{ Html::script('assets/scripts/auto_complete/jquery.min.js'); }}         
{{ Html::script('assets/scripts/auto_complete/jquery-ui.min.js'); }}
<script>
        // Put jQuery 1.10.2 into noConflict mode.
    var $jq1 = jQuery.noConflict(true);
</script>
{{ Html::script('assets/scripts/view_member.js'); }}
<link rel="stylesheet" href="http://demo.expertphp.in/css/jquery.treeview.css" />
<script src="http://demo.expertphp.in/js/jquery-treeview.js"></script>
<script type="text/javascript" src="http://demo.expertphp.in/js/demo.js"></script>

<div class="col-sm-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                Please Correct the following errors.
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
         @endif
         <div id="divStatus">             
         </div>
<div class="row">
    {{ Form::open(['url'=>'searchMember','id'=>'searchMember','role'=>'form']) }}
    <div class="col-lg-6">
            <div class="form-group">
                <label>Full Name</label>
                <input name="txtFullName" id="txtFullName" class="form-control" placeholder="Enter full Name" value="{{old('txtFullName')}}" />
            </div>            
            <div class="form-group">
                <label>Email Address</label>
                <input name="txtEmail" id="txtEmail" class="form-control" placeholder="Enter valid email" value="{{old('txtEmail')}}" />
            </div>
            <div class="form-group">
                <label>AIA Agent ID</label>
                    <input name="txtAgentId" id="txtAgentId" class="form-control" placeholder="Enter agent ID" value="{{old('txtAgentId')}}" />
            </div>            
    </div> 
    <div class="col-lg-12">
        &nbsp;
    </div>
    <div class="col-lg-12">
        <input type="button" id="btnSearch" class="btn btn-success" value="Search"></input>
        <input type="reset" class="btn btn-danger" value="Clear Form" />
    </div>
    <div class="col-lg-12">
        <br />
    </div>
    <div class="col-lg-12">
        <table class="table table-striped table-bordered table-hover" id="tblMembers">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Agent ID</th>
                <th>Actions</th>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    <!-- Modal (Pop up when detail button clicked) -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Update Agent</h4>
                </div>
                <div class="modal-body">
                    <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                        <div class="col-lg-12">  
                            <div id="statusMsg"></div>
                            <div class="form-group">
                                <label>ID</label>
                                <input name="txtIDEdit" id="txtIDEdit" class="form-control" readonly="true" />
                                
                            </div>  
                            <div class="form-group">
                                <label>Full Name</label>
                                <input name="txtFullNameEdit" id="txtFullNameEdit" class="form-control" placeholder="Enter full Name" value="{{old('txtFullName')}}" />
                                <div class="text-danger" id="divFullNameEdit"></div>
                            </div>            
                            <div class="form-group">
                                <label>Email Address</label>
                                <input name="txtEmailEdit" id="txtEmailEdit" class="form-control" placeholder="Enter valid email" value="{{old('txtEmail')}}" />
                                <div class="text-danger" id="divEmailEdit"></div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label><br />
                                <select name="slPrefixEdit" id="slPrefixEdit" class="form-control" style="width: 20%; display: inline">
                                    <option value="010">010</option>
                                    <option value="011">011</option>
                                    <option value="012">012</option>
                                    <option value="013">013</option>
                                    <option value="015">015</option>
                                    <option value="016">016</option>
                                    <option value="017">017</option>
                                    <option value="018">018</option>
                                    <option value="019">019</option>
                                </select>
                                <input name="txtPhoneNumberEdit" id="txtPhoneNumberEdit" class="form-control" style="width: 79%; display: inline" placeholder="E.g. 191337899" value="{{old('txtPhoneNumber')}}" />
                                <div class="text-danger" id="divPhoneNumberEdit"></div>
                            </div>
                            <div class="form-group">
                                <label>AIA Agent ID</label>
                                    <input name="txtAgentIdEdit" id="txtAgentIdEdit" class="form-control" placeholder="Enter agent ID" value="{{old('txtAgentIdEdit')}}" />
                                    <div class="text-danger" id="divAgentIdEdit"></div>
                            </div>
                            <div class="form-group">
                                <label>Upline</label>
                                    <input name="txtUplineEdit" id="txtUplineEdit" class="form-control" placeholder="Enter agent ID" value="{{old('txtUplineEdit')}}" />
                                    
                            </div>
                    </div>  
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                    <input type="hidden" id="task_id" name="task_id" value="0">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalTree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabelTree">Agents Tree</h4>
                </div>
                <div class="modal-body">
                    <form id="frmTasks2" name="frmTasks2" class="form-horizontal" novalidate="">
                        <div class="col-lg-12">  
                            <div id="statusMsg2"></div>
                                <div class="panel panel-primary">
                                    <div class="panel-heading" id="divAgentTreeHeader"></div>
                                      <div class="panel-body" id="divTheThree">  
                                          
                                      </div>
                                </div>  
                        </div>  
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="passwordModalLabel">Reset Agent Password: </h4>
                </div>
                <div class="modal-body">
                        <div class="col-lg-12">  
                            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                            <div id="statusMsgPwd"></div>
                            <div class="form-group">
                                <label>Password</label>
                                <input name="txtPassword" id="txtPassword" class="form-control" type="password" placeholder="Combination of Alphabet and Numeric only" value="{{old('txtPassword')}}" />
                                <div class="text-danger" id="divPasswordEdit"></div>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input name="txtCPassword" id="txtCPassword" class="form-control" placeholder="Combination of Alphabet and Numeric only"  type="password" value="{{old('txtCPassword')}}" />
                                <div class="text-danger" id="divCPasswordEdit"></div>
                            </div>
                            </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save-password" value="add">Update Password</button>
                    <input type="hidden" id="task_id" name="task_id" value="0">
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
    <div class="row">
        &nbsp;
    </div>
</div>

@stop