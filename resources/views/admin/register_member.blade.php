@extends('layouts.dashboard')
@section('page_heading','Register Agent')

@section('section')
<link rel="stylesheet" href="assets/scripts/auto_complete/jquery-ui.css" type="text/css" />
{{ Html::script('assets/scripts/auto_complete/jquery.min.js'); }}         
{{ Html::script('assets/scripts/auto_complete/jquery-ui.min.js'); }}
<script>
        // Put jQuery 1.10.2 into noConflict mode.
    var $jq1 = jQuery.noConflict(true);
</script>
{{ Html::script('assets/scripts/create_member.js'); }}
<div class="col-sm-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                Please Correct the following errors.
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
         @endif
<div class="row">
    {{ Form::open(['url'=>'create_member','id'=>'signup','role'=>'form']) }}
    <div class="col-lg-6">
            <div class="form-group">
                <label>Full Name</label>
                <input name="txtFullName" id="txtFullName" class="form-control" placeholder="Enter full Name" value="{{old('txtFullName')}}" />           
                @if ($errors->has('txtFullName'))
                    <div class="text-danger">{{ $errors->first('txtFullName', ':message'); }}</div>
                @endif
            </div>            
            <div class="form-group">
                <label>Email Address</label>
                <input name="txtEmail" id="txtEmail" class="form-control" placeholder="Enter valid email" value="{{old('txtEmail')}}" />
                @if ($errors->has('txtEmail'))
                    <div class="text-danger">{{ $errors->first('txtEmail', ':message'); }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Phone Number</label><br />
                <select name="slPrefix" id="slPrefix" class="form-control" style="width: 20%; display: inline">
                    <option value="010">010</option>
                    <option value="011">011</option>
                    <option value="012">012</option>
                    <option value="013">013</option>
                    <option value="015">015</option>
                    <option value="015">016</option>
                    <option value="015">017</option>
                    <option value="015">018</option>
                    <option value="015">019</option>
                </select>
                <input name="txtPhoneNumber" id="txtPhoneNumber" class="form-control" style="width: 79%; display: inline" placeholder="E.g. 191337899" value="{{old('txtPhoneNumber')}}" />
                @if ($errors->has('txtPhoneNumber'))
                    <div class="text-danger">{{ $errors->first('txtPhoneNumber', ':message'); }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Upline</label>
                    <input name="txtUpline" id="txtUpline" class="form-control" placeholder="Enter upline" value="{{old('txtUpline')}}" />
                    @if ($errors->has('txtUpline'))
                        <div class="text-danger">{{ $errors->first('txtUpline', ':message'); }}</div>
                    @endif
            </div>
            
    </div>    
    <div class="col-lg-6">
        <div class="form-group">
            <label>AIA Agent ID</label>
                <input name="txtAgentId" id="txtAgentId" class="form-control" placeholder="Enter agent ID" value="{{old('txtAgentId')}}" />
                @if ($errors->has('txtAgentId'))
                    <div class="text-danger">{{ $errors->first('txtAgentId', ':message'); }}</div>
                @endif
                        
        </div>
        <div class="form-group">
            <label>Password</label>
            <input name="txtPassword" id="txtPassword" class="form-control" type="password" placeholder="Combination of Alphabet and Numeric only" value="{{old('txtPassword')}}" />
            @if ($errors->has('txtPassword'))
                <div class="text-danger">{{ $errors->first('txtPassword', ':message'); }}</div>
            @endif
        </div>
        <div class="form-group">
            <label>Confirm Password</label>
            <input name="txtCPassword" id="txtCPassword" class="form-control" placeholder="Combination of Alphabet and Numeric only"  type="password" value="{{old('txtCPassword')}}" />
            @if ($errors->has('txtCPassword'))
                <div class="text-danger">{{ $errors->first('txtCPassword', ':message'); }}</div>
            @endif
        </div>
    </div>
    <div class="col-lg-12">
        &nbsp;
    </div>
    <div class="col-lg-12">
        <button type="submit" class="btn btn-success">Submit</button>
        <input type="reset" class="btn btn-danger" value="Clear Form" />
    </div>
    {{ Form::close() }}
</div>
    <div class="row">
        &nbsp;
    </div>
</div>
@stop