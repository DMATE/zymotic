@extends('layouts.dashboard')
@section('page_heading','Customer Page')

@section('section')
<link rel="stylesheet" href="../assets/scripts/auto_complete/jquery-ui.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/b-1.2.4/b-html5-1.2.4/b-print-1.2.4/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.4/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/b-1.2.4/b-html5-1.2.4/b-print-1.2.4/datatables.min.js"></script>
<script>
        // Put jQuery 1.10.2 into noConflict mode.
    var $jq1 = jQuery.noConflict(true);
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_XEiFkXKfSx5xPemcDtYulrsYvXwTu78&libraries=places&sensor=false" type="text/javascript"></script>
{{ Html::script('../assets/scripts/auto_complete/jquery.min.js'); }}         
{{ Html::script('../assets/scripts/auto_complete/jquery-ui.min.js'); }}
<script>
        // Put jQuery 1.10.2 into noConflict mode.
    var $jq2 = jQuery.noConflict(true);
</script>
{{ Html::script('../assets/scripts/view_customer.js'); }}
<div class="col-sm-12">
<div class="row">
    <div class="row">
        <!-- /.row -->
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Customer Information
                </div>
                <div class="panel-body">
                    <div class="col-sm-3 text-info">Full Name </div>
                    <div class="col-sm-7">{{ $something->name }}</div>
                    <div class="col-sm-3 text-info">Phone Number </div>
                    <div class="col-sm-7">{{ $something->phoneprefix.'-'.$something->phone }}</div>
                    <div class="col-sm-3 text-info">Address </div>
                    <div class="col-sm-7">{{ $something->address }}&nbsp;</div>
                    <div class="col-sm-3 text-info">Agent </div>
                    <div class="col-sm-7">{{ $something->agent->name }}</div>
                </div>
                <div class="panel-footer">
                    <input type='button' value='Edit' class='btn btn-warning edit' id='edit{{$something->id}}'>
                    <input type='button' value='Create Activities' class='btn btn-success actions' id='action{{$something->id}}'>
                </div>
            </div>
        </div>
            
    </div>
    <div class="row">
        &nbsp;
    </div>
    <div class="row">
        <!-- /.row -->
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Policies
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th>No</th>
                            <th>Policy Number</th>
                            <th>Insured</th>
                            <th>Start Date</th>
                            <th>ANP</th>
                            <th>FYP</th>
                            <th>Mode</th>
                            <th>Terms</th>
                            <th>Update</th>
                        </thead>
                        <tbody>
                        <?php @$i = 1 ?>
                        @foreach($something->policy as $p)
                        <?php 
                            if ($p->policyterm == 'Monthly')
                            {
                                $date = DateTime::createFromFormat('d/m/Y', $p->startdate);
                                $date->add(new DateInterval('P1M'));
                                $date = $date->format('d/m/Y');
                            }
                            else if ($p->policyterm == 'Quarterly')
                                {
                                $date = DateTime::createFromFormat('d/m/Y', $p->startdate);
                                $date->add(new DateInterval('P3M'));
                                $date = $date->format('d/m/Y');
                            }
                            else
                            {
                                $date = $p->startdate;
                            }
                        ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $p->policyid }}</td>
                            <td>{{ $p->insuredname }}</td>
                            <td>{{ $p->startdate }}</td>
                            <td>{{ $p->anp }}</td>
                            <td>{{ $p->fyp }}</td>
                            <td>{{ $p->policymode }}</td>
                            <td>{{ $p->policyterm }}</td>
                            <td><input type='button' value='Policy' class='btn btn-success policy' id='policy{{$p->id}}'></td>
                        </tr>
                        <?php $i++ ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Activities
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id='tblActivity'>
                        <thead>
                            <th>No</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Potential ANP</th>
                            <th>ANP</th>
                            <th>Referred</th>
                            <th>Recruit</th>
                            <th>Contact</th>
                            <th>View</th>
                        </thead>
                        <tbody>
                        <?php $i = 1 ?>
                        @foreach($something->activity as $p)
                        <?php
                            $anp = '';
                            if ($p ->label == 'Deal')
                            {
                                foreach($something->policy as $b)
                                {
                                    if ($b->activityid == $p->id)
                                    {
                                        $anp = $b->anp;
                                    }
                                }
                            }                        
                        ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $p->activitydatetime }}</td>
                            <td>{{ $p->label }}</td>
                            <td>{{ $p->potentialanp }}</td>
                            <td>{{ $anp }}</td>
                            <td>{{ $p->numberrefer }}</td>
                            <td>{{ $p->recruit }}</td>
                            <td>{{ $p->recruitphone }}</td>
                            <td><input type='button' value='Action' class='btn btn-success luaction' id='luaction{{$p->id}}'></td>
                        </tr>
                        <?php $i++ ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalEditLabel">Update Customer</h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" name="frmEdit" class="form-horizontal" novalidate="">
                        <div class="col-lg-12">  
                            <div id="statusMsgEdit"></div>
                            <div class="form-group">
                                <label>ID</label>
                                <input name="txtIDEdit" id="txtIDEdit" class="form-control" readonly="true" />
                                
                            </div>  
                            <div class="form-group">
                                <label>Full Name</label>
                                <input name="txtFullNameEdit" id="txtFullNameEdit" class="form-control" placeholder="Enter full Name" value="{{old('txtFullName')}}" />
                                <div class="text-danger" id="divFullNameEdit"></div>
                            </div> 
                            <div class="form-group">
                                <label>Phone Number</label><br />
                                <select name="slPrefixEdit" id="slPrefixEdit" class="form-control" style="width: 20%; display: inline">
                                    <option value="010">010</option>
                                    <option value="011">011</option>
                                    <option value="012">012</option>
                                    <option value="013">013</option>
                                    <option value="015">015</option>
                                    <option value="016">016</option>
                                    <option value="017">017</option>
                                    <option value="018">018</option>
                                    <option value="019">019</option>
                                </select>
                                <input name="txtPhoneNumberEdit" id="txtPhoneNumberEdit" class="form-control" style="width: 79%; display: inline" placeholder="E.g. 191337899" value="{{old('txtPhoneNumberEdit')}}" />
                                <div class="text-danger" id="divPhoneNumberEdit"></div>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input name="txtAddressEdit" id="txtAddressEdit" class="form-control" placeholder="Enter address" value="{{old('txtAddressEdit')}}" />
                                <div class="text-danger" id="divAddressEdit"></div>
                            </div>
                            <div class="form-group">
                                <label>Agent *</label>
                                <input name="txtAgentEdit" id="txtAgentEdit" class="form-control" placeholder="Part of Agent Name / AIA ID" value="{{old('txtAgentEdit')}}" />           
                                <div class="text-danger" id="divAgentEdit"></div>
                            </div>
                    </div>  
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-saveEdit" value="Save">Save changes</button>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modalAction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalActionLabel">Actions Done</h4>
                </div>
                <div class="modal-body">
                    <form id="frmAction" name="frmAction" class="form-horizontal" novalidate="">
                        <div class="col-lg-12">  
                            <div id="statusMsg"></div>
                            <div class="form-group">
                                <label>Actions</label>
                                <select name="slActions" id="slActions" class="form-control">
                                    <option value="Visit">Visit (V)</option>
                                    <option value="Approach">Approach (A)</option>
                                    <option value="Referral">Referral (R)</option>
                                    <option value="Deal">Deal (D)</option>
                                    <option value="Recruit">Recruit (R)</option>
                                </select>                                
                            </div>  
                            <div class="form-group">
                                <label>Date</label>
                                <input name="txtDate" id="txtDate" class="form-control datepicker" value="{{old('txtDate')}}" />
                                <div class="text-danger" id="divDate"></div>
                            </div>            
                            <div class="form-group" id="fgPotentialANP" Style="display: none">
                                <label>Potential ANP</label>
                                <input name="txtPotentialANP" id="txtPotentialANP" class="form-control" placeholder="Enter potential ANP" value="{{old('txtPotentialANP')}}" />
                                <div class="text-danger" id="divPotentialANP"></div>
                            </div> 
                            <div class="form-group" id="fgNumberRef" Style="display: none">
                                <label>Number of referrals</label>
                                <input name="txtNumberRef" id="txtNumberRef" class="form-control" placeholder="Enter number of referrals" value="{{old('txtNumberRef')}}" />
                                <div class="text-danger" id="divNumberRef"></div>
                            </div>
                            <div class="form-group closeGroup" Style="display: none">
                                <label>Insured Name</label>
                                <input name="txtInsuredName" id="txtInsuredName" class="form-control" placeholder="Enter insured name" value="{{old('txtInsuredName')}}" />
                                <div class="text-danger" id="divInsuredName"></div>
                            </div>
                            <div class="form-group closeGroup" Style="display: none">
                                <label>ANP Amount</label>
                                <input name="txtANPAmount" id="txtANPAmount" class="form-control" placeholder="Enter ANP Amount" value="{{old('txtANPAmount')}}" />
                                <div class="text-danger" id="divANPAmount"></div>
                            </div>
                            <div class="form-group closeGroup" Style="display: none">
                                <label>FYP Amount</label>
                                <input name="txtFYPAmount" id="txtFYPAmount" class="form-control" placeholder="Enter FYP Amount" value="{{old('txtFYPAmount')}}" />
                                <div class="text-danger" id="divFYPAmount"></div>
                            </div>
                            <div class="form-group closeGroup" Style="display: none">
                                <label>Policy Start Date</label>
                                <input name="txtStartDate" id="txtStartDate" class="form-control datepicker" value="{{old('txtStartDate')}}" />
                                <div class="text-danger" id="divStartDate"></div>
                            </div>
                            <div class="form-group closeGroup" Style="display: none">
                                <label>Payment Frequency</label>
                                <select id="slPaymentFrequency" class="form-control" value="{{old('slPaymentFrequency')}}">
                                    <option value="">Please Select Payment Frequency</option>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Quarterly">Quarterly</option>
                                    <option value="Semi-Annual">Semi-Annual</option>
                                    <option value="Annual">Annual</option>
                                </select>
                                <div class="text-danger" id="divPaymentFrequency"></div>
                            </div>
                            <div class="form-group closeGroup" Style="display: none">
                                <label>Payment Mode</label>
                                <select id="slPaymentMode" class="form-control" value="{{old('slPaymentMode')}}">
                                    <option value="">Please Select Payment Mode</option>
                                    <option value="Direct Pay">Direct Pay</option>
                                    <option value="Credit Card">Credit Card</option>
                                    <option value="Debit Card">Debit Card</option>
                                    <option value="Autopay">Autopay</option>
                                </select>
                                <div class="text-danger" id="divPaymentMode"></div>
                            </div>
                            <div class="form-group rec" id="fgNewRecruit" Style="display: none">
                                <label>Potential New Recruit</label>
                                <input name="txtPotentialNewRecruit" id="txtPotentialNewRecruit" class="form-control" placeholder="Enter potential new recruit name" value="{{old('txtPotentialNewRecruit')}}" />
                                <div class="text-danger" id="divPotentialNewRecruit"></div>
                            </div>
                            <div class="form-group rec" id="fgNewRecruit" Style="display: none">
                                <label>New Recruit Contact</label>
                                <input name="txtNewRecruitPhone" id="txtNewRecruitPhone" class="form-control" placeholder="Enter potential new recruit contact" value="{{old('txtNewRecruitPhone')}}" />
                                <div class="text-danger" id="divNewRecruitPhone"></div>
                            </div>
                            <div class="form-group" id="fgLocation">
                                <label>Check In</label>
                                <input name="txtLocationSearch" id="txtLocationSearch" class="form-control" placeholder="Search place here"  />                                
                            </div>
                            <div id="map" style="height: 300px"></div><br />
                            <div class="form-group" id="fgLocation">
                                <label>Attach Picture</label> 
                                <div id="divPicMain" class="col-md-12 text-center"
                                 style="border: 1px solid lightgray; height: 240px">
                                     <div id="divUpload" style="margin-top: 100px">
                                         <input type="file" id="fuImage" style="display: none">
                                         <button type="button" class="btn btn-primary" id="btn-upload" onclick="showBrowseDialog()" value="Add">Browse</button>
                                     </div>
                                    <div id="divImage" style="display: none">
                                        <img id="imgUpload" style="height: 180px; margin-top: 10px" />
                                        <br /><button type="button" id="btnDeletePhoto" style="margin-top: 6px" class="btn btn-danger" value="deletePhoto">Remove</button>
                                        <input type="hidden" id="divHidden" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="fgRemark">
                                <label>Remark</label>
                                <input name="txtRemark" id="txtRemark" class="form-control" placeholder="Enter remark" value="{{old('txtRemark')}}" />
                                <div class="text-danger" id="divRemark"></div>
                            </div>
                            <input id="txtLat" type="hidden" value="0" />
                            <input id="txtLng" type="hidden" value="0" />
                    </div>  
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
                    <input type="hidden" id="action_cust_id" name="action_cust_id" value="0">
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modalPolicy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalPolicyLabel">Update Customer</h4>
                </div>
                <div class="modal-body">
                    <form id="frmEdit" name="frmPolicy" class="form-horizontal" novalidate="">
                        <div class="col-lg-12">  
                            <div id="statusMsgPolicy"></div>
                            <div class="form-group">
                                <label>Policy Number</label>
                                <input name="txtPolicyNumberPolicy" id="txtPolicyNumberPolicy" class="form-control" placeholder="Enter policy number" value="{{old('txtPolicyNumberPolicy')}}" />
                                <div class="text-danger" id="divPolicyNumberPolicy"></div>
                            </div>
                            <div class="form-group">
                                <label>Insured Name</label>
                                <input name="txtInsuredNamePolicy" id="txtInsuredNamePolicy" class="form-control" placeholder="Enter insured name" value="{{old('txtInsuredNamePolicy')}}" />
                                <div class="text-danger" id="divInsuredNamePolicy"></div>
                            </div>
                            <div class="form-group">
                                <label>ANP Amount</label>
                                <input name="txtANPAmountPolicy" id="txtANPAmountPolicy" class="form-control" placeholder="Enter ANP Amount" value="{{old('txtANPAmountPolicy')}}" />
                                <div class="text-danger" id="divANPAmountPolicy"></div>
                            </div>
                            <div class="form-group">
                                <label>FYP Amount</label>
                                <input name="txtFYPAmountPolicy" id="txtFYPAmountPolicy" class="form-control" placeholder="Enter FYP Amount" value="{{old('txtFYPAmountPolicy')}}" />
                                <div class="text-danger" id="divFYPAmountPolicy"></div>
                            </div>
                            <div class="form-group">
                                <label>Policy Start Date</label>
                                <input name="txtStartDatePolicy" id="txtStartDatePolicy" class="form-control datepicker" value="{{old('txtStartDatePolicy')}}" />
                                <div class="text-danger" id="divStartDatePolicy"></div>
                            </div>
                            <div class="form-group">
                                <label>Payment Frequency</label>
                                <select id="slPaymentFrequencyPolicy" class="form-control" value="{{old('slPaymentFrequencyPolicy')}}">
                                    <option value="">Please Select Payment Frequency</option>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Quarterly">Quarterly</option>
                                    <option value="Semi-Annual">Semi-Annual</option>
                                    <option value="Annual">Annual</option>
                                </select>
                                <div class="text-danger" id="divPaymentFrequencyPolicy"></div>
                            </div>
                            <div class="form-group">
                                <label>Payment Mode</label>
                                <select id="slPaymentModePolicy" class="form-control" value="{{old('slPaymentModePolicy')}}">
                                    <option value="">Please Select Payment Mode</option>
                                    <option value="Direct Pay">Direct Pay</option>
                                    <option value="Credit Card">Credit Card</option>
                                    <option value="Debit Card">Debit Card</option>
                                    <option value="Autopay">Autopay</option>
                                </select>
                                <div class="text-danger" id="divPaymentModePolicy"></div>
                    </div>  
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-savePolicy" value="Save">Save Policy</button>
                    <input type="hidden" id="policy_id" name="policy_id" value="0">
                </div>
            </div>
        </div>
    </div>
@stop