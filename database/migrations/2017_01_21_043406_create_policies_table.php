<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('activityid');
            $table->string('policyid');
            $table->integer('customer');
            $table->integer('agent');
            $table->string('policyremark');
            $table->string('startdate');
            $table->string('policyterm');
            $table->string('policymode');
            $table->string('insuredname');
            $table->string('updatedby');
            $table->float('anp');
            $table->float('fyp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policies');
    }
}
