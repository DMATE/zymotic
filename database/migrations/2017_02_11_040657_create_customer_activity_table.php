<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('customerid');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('photo');
            $table->string('remarks');
            $table->integer('numberrefer');
            $table->string('recruit');
            $table->string('recruitphone');
            $table->datetime('activitydatetime');
            $table->float('potentialanp', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_activities');
    }
}
