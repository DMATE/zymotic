<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('terms')->delete();
        
        Model::unguard();
        $monthly = ['fullname' => 'MONTHLY', 'months' => 1];
        $quarterly = ['fullname' => 'QUARTERLY', 'months' => 3];
        $semiannual = ['fullname' => 'SEMI-ANNUAL', 'months' => 6];
        $annual = ['fullname' => 'ANNUALLY', 'months' => 12];
        $db = DB::table('terms')->insert($monthly);
        $db = DB::table('terms')->insert($quarterly);
        $db = DB::table('terms')->insert($semiannual);
        $db = DB::table('terms')->insert($annual);
        
        DB::table('mode')->delete();
        
        Model::unguard();
        $monthly = ['fullname' => 'MONTHLY', 'months' => 1];
        $quarterly = ['fullname' => 'QUARTERLY', 'months' => 3];
        $semiannual = ['fullname' => 'SEMI-ANNUAL', 'months' => 6];
        $annual = ['fullname' => 'ANNUALLY', 'months' => 12];
        $db = DB::table('terms')->insert($monthly);
        $db = DB::table('terms')->insert($quarterly);
        $db = DB::table('terms')->insert($semiannual);
        $db = DB::table('terms')->insert($annual);
    }
}
