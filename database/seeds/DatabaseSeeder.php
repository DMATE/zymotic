<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
                $this->call('AdminRoleSeeder');
		// $this->call('UserTableSeeder');
	}

}

class AdminRoleSeeder extends Seeder {
    //put your code here
    public function run()
	{
                DB::table('adminroles')->delete();
                DB::table('admins')->delete();
        
		Model::unguard();
                $role_super_admin = ['name' => 'super_admin'];
                $role_owner = ['name' => 'system_owner'];
                $role_normal = ['name' => 'normal_admin'];
                $db = DB::table('adminroles')->insert($role_super_admin);
                $db = DB::table('adminroles')->insert($role_owner);
                $db = DB::table('adminroles')->insert($role_normal);
		// $this->call('UserTableSeeder');
                
                $firstRole = DB::table('adminroles')
                    ->where('name', '=', 'super_admin')
                    ->select('id')->first();
                $admin =
                    [
                        'name' => 'DMATE Enterprise',
                        'email' => 'dmateenterprise@gmail.com',
                        'password' => bcrypt('dmate@12345'),
                        'role' => $firstRole -> id
                    ];
                $db = DB::table('admins')->insert($admin);
	}
}
