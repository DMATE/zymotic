/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {   
    
        
    
        $jq1("#txtFullName").autocomplete({
           source: function(request, response) {
               $.ajax({
                   type: 'POST',
                   url: '/searchByName',
                   dataType: "json",
                   data: {
                       term : request.term,
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                     response($.map(data, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1],
                                    em: item.split('|')[2]
                                }
                            }))
                   }
               });
           },
           min_length: 1,
           select: function (e, i) {
                    $("#txtAgentId").val(i.item.val);
                    $('#txtEmail').val(i.item.em);
                },
       });
       
       $jq1("#txtUplineEdit").autocomplete({
           source: function(request, response) {
               $.ajax({
                   type: 'POST',
                   url: '/searchByNameId',
                   dataType: "json",
                   data: {
                       term : request.term,
                       _token : $('input[name="_token"]').val()
                   },                   
                   success: function(data) {
                     response($.map(data, function (item) {
                                return item.split('|')[0] + " - " +
                                        item.split('|')[1];
                            }))
                   }
               });
           },
           appendTo : $('#myModal'),
           min_length: 1
       });
       
       $("#btnSearch").click( function()
       {
           
           $.ajax({
                   type: 'POST',
                   url: '/searchMember',
                   dataType: "json",
                   data: {
                       name: $('input[name="txtFullName"]').val(),
                       email: $('input[name="txtEmail"]').val(),
                       agentId: $('input[name="txtAgentId"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       displayMembers(data);
                   }
               });
       });
       
       $('#btn-save').click(function()
       {
           $('#divFullNameEdit').html('');
           $('#divEmailEdit').html('');
           $('#divPhoneNumberEdit').html('');
           $('#divAgentIdEdit').html('');
           $('#statusMsg').html('');
           
           $.ajax({
                   type: 'POST',
                   url: '/updateAgent',
                   dataType: "json",
                   data: {
                       name: $('input[name="txtFullNameEdit"]').val(),
                       email: $('input[name="txtEmailEdit"]').val(),
                       phonenumber: $('input[name="txtPhoneNumberEdit"]').val(),
                       agentid: $('input[name="txtAgentIdEdit"]').val(),
                       uplineid: $('input[name="txtUplineEdit"]').val(),
                       phoneprefix: $('#slPrefixEdit').val(),
                       id: $('input[name="txtIDEdit"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       if (data.success == "true")
                       {
                           $('#divFullNameEdit').html();
                           $('#divEmailEdit').html();
                           $('#divPhoneNumberEdit').html();
                           $('#divAgentIdEdit').html();
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'name')
                           {
                               $('#divFullNameEdit').html(member2);
                           }
                           if (elem == 'email')
                           {
                               $('#divEmailEdit').html(member2);
                           }
                           if (elem == 'phonenumber')
                           {
                               $('#divPhoneNumberEdit').html(member2);
                           }
                           if (elem == 'agentid')
                           {
                               $('#divAgentIdEdit').html(member2);
                           }
                        })                       
                   }
               });
       });
       
       $('#btn-save-password').click(function()
       {
           $('#statusMsgPwd').html('');
           $('#divPasswordEdit').html('');
           $('#divCPasswordEdit').html('');
           
           $.ajax({
                   type: 'POST',
                   url: '/updateAgentPassword',
                   dataType: "json",
                   data: {
                       password: $('input[name="txtPassword"]').val(),
                       cpassword: $('input[name="txtCPassword"]').val(),
                       id: $('input[name="task_id"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       if (data.success == "true")
                       {
                           $('#divPasswordEdit').html('');
                           $('#divCPasswordEdit').html('');
                           $('#statusMsgPwd').attr("class", "alert alert-info");
                           $('#statusMsgPwd').html(data.message);
                       }
                       else
                       {
                           $('#statusMsgPwd').attr("class", "alert alert-danger");
                           $('#statusMsgPwd').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsgPwd').attr("class", "alert alert-danger");
                       $('#statusMsgPwd').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'password')
                           {
                               $('#divPasswordEdit').html(member2);
                           }
                           if (elem == 'cpassword')
                           {
                               $('#divCPasswordEdit').html(member2);
                           }
                        })                       
                   }
               });
       });
});

function displayMembers(data)
{
    $("#divStatus").html("<p class='alert alert-success' id='pSuccess'>Agents retrieved successfully.</p>");
    var tbody = $("#tblMembers tbody");
    tbody.empty();
    var tag_start = "<td>";
    var tag_end = "</td>";
    
    $.each(data.members, function(elem, member){
            var tr = $("<tr>");
            tr.append(tag_start + member.id + tag_end);
            tr.append(tag_start + member.name + tag_end);
            tr.append(tag_start + member.email + tag_end);
            tr.append(tag_start + member.phoneprefix + '-' + member.phone + tag_end);
            tr.append(tag_start + member.agentid + tag_end);
            tr.append("<td><input type='button' value='Edit' class='btn btn-warning edit' id=edit" + member.id + ">\
    &nbsp;<input type='button' value='Tree' class='btn btn-success agenttree' id=tree" + member.id + ">\
    &nbsp;<input type='button' value='Pwd' class='btn btn-success cpassword' id=pwd" + member.id + "></td>");
            tbody.append(tr);
        })
        
    $('.edit').click(function(event){
        
            var member_id = event.target.id.replace("edit","");
            var url = "/getMember/" + member_id;
            //alert(member_id)
            //$('#myModal').modal('show');
            
            $.get(url, function (data2) {
                
                $.each(data2.member, function(elem, member2){
                    $('#txtFullNameEdit').val(member2.name);
                    $('#txtEmailEdit').val(member2.email);
                    $('#slPrefixEdit').val(member2.phoneprefix);
                    $('#txtPhoneNumberEdit').val(member2.phone);
                    $('#txtAgentIdEdit').val(member2.agentid);
                    $('#txtUplineEdit').val(member2.uplinename + " - " + member2.uplineid);
                    $('#txtIDEdit').val(member2.id);
                })
                
                $('#myModal').modal('show');
            }) 
            
            
        });
        
    $('.agenttree').click(function(event){
        
        var member_id = event.target.id.replace("tree","");
        var url = "/getTree/" + member_id;
        
       
        $.get(url, function (data2) {
            $('#divAgentTreeHeader').html(data2.name);
            $('#divTheThree').html(data2.tree);
            $('#myModalTree').modal('show');
          
        })   
            
    });
    
    
    $('.cpassword').click(function(event){
        
        var member_id = event.target.id.replace("pwd","");
        var url = "/getMember/" + member_id;
        
       
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
            $('#passwordModalLabel').html("Reset password for: " + member2.name);
            $('#task_id').val(member2.id);
            $('#passwordModal').modal('show');
            })
        })  
    });
}

