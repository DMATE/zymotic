/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $jq1('#tblActivity').DataTable(
            {
                 responsive: true
            });
            
    $jq2( function() {
        $jq2( "#txtDate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
          }).datepicker("setDate", "0");
          
        $jq2( "#txtStartDate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
          }).datepicker("setDate", "0");
        } );
            
    $jq2("#txtAgentEdit").autocomplete({
           source: function(request, response) {
               $.ajax({
                   type: 'POST',
                   url: '/searchAgent',
                   dataType: "json",
                   data: {
                       term : request.term,
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                     response($.map(data, function (item) {
                                return item.split('|')[0] + " - " +
                                        item.split('|')[1] + " - " + 
                                        item.split('|')[2];
                            }))
                   }
               });
           },
           min_length: 1
       });
            
    $('.edit').click(function(event){
        
        var member_id = event.target.id.replace("edit","");
        var url = "/getCustomer/" + member_id;        
       
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
            $('#modalEditLabel').html("Edit: " + member2.name);
            //$('#task_id').val(member2.id);
            
            $('#txtIDEdit').val(member2.id);
            $('#txtFullNameEdit').val(member2.name);
            $('#slPrefixEdit').val(member2.phoneprefix);
            $('#txtPhoneNumberEdit').val(member2.phone);
            $('#txtAgentEdit').val(member2.agent.name + ' - ' + member2.agent.agentid + ' - ' + member2.agent.id);
            $('#txtAddressEdit').val(member2.address);
            
            $('#modalEdit').modal('show');
            })
        })  
    });
    
    $('#btn-saveEdit').click(function()
        {
           $('#divFullNameEdit').html('');
           $('#divEmailEdit').html('');
           $('#divPhoneNumberEdit').html('');
           $('#divAgentEdit').html('');
           $('#divAddressEdit').html('');
           $('#statusMsg').html('');
           
           $.ajax({
                   type: 'POST',
                   url: '/updateCustomer',
                   dataType: "json",
                   data: {
                       name: $('input[name="txtFullNameEdit"]').val(),
                       phonenumber: $('input[name="txtPhoneNumberEdit"]').val(),
                       phoneprefix: $('#slPrefixEdit').val(),
                       agent: $('#txtAgentEdit').val(),
                       address: $('#txtAddressEdit').val(),
                       id: $('input[name="txtIDEdit"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#divFullNameEdit').html();
                           $('#divPhoneNumberEdit').html();
                           $('#divAgentEdit').html();
                           $('#statusMsgEdit').attr("class", "alert alert-info");
                           $('#statusMsgEdit').html(data.message);
                       }
                       else
                       {
                           $('#statusMsgEdit').attr("class", "alert alert-danger");
                           $('#statusMsgEdit').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsgEdit').attr("class", "alert alert-danger");
                       $('#statusMsgEdit').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'name')
                           {
                               $('#divFullNameEdit').html(member2);
                           }
                           if (elem == 'phonenumber')
                           {
                               $('#divPhoneNumberEdit').html(member2);
                           }
                           if (elem == 'agent')
                           {
                               $('#divAgentEdit').html(member2);
                           }
                        })                       
                   }
               });
       });
       
    $('#btn-save').click(function()
        {
           $('#divDate').html('');
           $('#statusMsg').html('');
           $('#divANPAmount').html('');
            $('#divStartDate').html('');
            $('#divInsuredName').html('');
            $('#divPaymentMode').html('');
            $('#divPaymentFrequency').html('');
            $('#divNumberRef').html('');
            $('#divPotentialNewRecruit').html('');
            $('#divNewRecruitPhone').html('');
           
           if (($('#slActions')).val() == 'Visit')
           {
                $url = '/createVisit';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);                           
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                        })                       
                   }
               });
           } 
           else if (($('#slActions')).val() == 'Approach')
           {
               $url = '/createApproach';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       potentialanp: $('#txtPotentialANP').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'potentialanp')
                           {
                               $('#divPotentialANP').html(member2);
                           }
                        })                       
                   }
               });
           }
           else if (($('#slActions')).val() == 'Deal')
           {
               $url = '/createDeal';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       anp: $('#txtANPAmount').val(),
                       fyp: $('#txtFYPAmount').val(),
                       paymentmode: $('#slPaymentMode').val(),
                       paymentfrequency: $('#slPaymentFrequency').val(),
                       insuredname: $('#txtInsuredName').val(),
                       policystart: $('#txtStartDate').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'anp')
                           {
                               $('#divANPAmount').html(member2);
                           }                           
                           else if (elem == 'fyp')
                           {
                               $('#divFYPAmount').html(member2);
                           }
                           else if (elem == 'policystart')
                           {
                               $('#divStartDate').html(member2);
                           }
                           else if (elem == 'insuredname')
                           {
                               $('#divInsuredName').html(member2);
                           }
                           else if (elem == 'paymentmode')
                           {
                               $('#divPaymentMode').html(member2);
                           }
                           else if (elem == 'paymentfrequency')
                           {
                               $('#divPaymentFrequency').html(member2);
                           }
                        })                       
                   }
               });
           }
           else if (($('#slActions')).val() == 'Referral')
           {
               $url = '/createReferral';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       numref: $('#txtNumberRef').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'numref')
                           {
                               $('#divNumberRef').html(member2);
                           }
                        })                       
                   }
               });
           }
           else if (($('#slActions')).val() == 'Recruit')
           {
               $url = '/createRecruit';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       recname: $('#txtPotentialNewRecruit').val(),
                       recphone: $('#txtNewRecruitPhone').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'recname')
                           {
                               $('#divPotentialNewRecruit').html(member2);
                           }
                           else if (elem == 'recphone')
                           {
                               $('#divNewRecruitPhone').html(member2);
                           }
                        })                       
                   }
               });
           }
        });
    $("#slActions").change(function() {
            if (this.value == "Approach")
            {
                $('#fgPotentialANP').css({'display' : 'block'});
            }
            else
            {
                $('#fgPotentialANP').css({'display' : 'none'});
            }
            
            if (this.value == "Referral")
            {
                $('#fgNumberRef').css({'display' : 'block'});
            }
            else
            {
                $('#fgNumberRef').css({'display' : 'none'});
            }
            
            if (this.value == "Deal")
            {
                $('.closeGroup').css({'display' : 'block'});
            }
            else
            {
                $('.closeGroup').css({'display' : 'none'});
            }
            
            if (this.value == "Recruit")
            {
                $('.rec').css({'display' : 'block'});
            }
            else
            {
                $('.rec').css({'display' : 'none'});
            }
            
        });
    $('.actions').click(function(event){
        var member_id = event.target.id.replace("action","");
        var url = "/getCustomer/" + member_id;        
       
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
            $('#modalActionLabel').html("Actions: " + member2.name);
            $('#action_cust_id').val(member2.id);
            $('#txtLat').val('3.139');
            $('#txtLng').val('101.6869');
            $('#btn-save').show(100);
            $('#modalAction').modal('show');
            
            resetImage();
            $('#frmAction')[0].reset();
            $('#slActions').val('Visit');
            })
        })  
    });
    
    
    $('#tblActivity').on('click', '.luaction', function(event){
        var member_id = event.target.id.replace("luaction","");
        var url = "/getActivity/" + member_id; 
       
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
            $('#modalActionLabel').html("Action: " + member2.id);
            $('#action_cust_id').val(member2.id);
            // do something
            if (member2.label == "Approach")
            {
                $('#fgPotentialANP').css({'display' : 'block'});
                $('#txtPotentialANP').val(member2.potentialanp);
            }
            else
            {
                $('#fgPotentialANP').css({'display' : 'none'});
            }
            
            if (member2.label == "Referral")
            {
                $('#fgNumberRef').css({'display' : 'block'});
                $('#txtNumberRef').val(member2.numberrefer);
            }
            else
            {
                $('#fgNumberRef').css({'display' : 'none'});
            }
            
            if (member2.label == "Deal")
            {
                $('.closeGroup').css({'display' : 'block'});
                
                var url = "/getPolicyByActivity/" + member2.id;  
                $.get(url, function (data2) {
                    $.each(data2.member, function(elem, member2){
                        $('#txtInsuredName').val(member2.insuredname);
                        $('#txtANPAmount').val(member2.anp);
                        $('#txtFYPAmount').val(member2.fyp);
                        $('#txtStartDate').val(member2.startdate);
                        $('#slPaymentFrequency').val(member2.policyterm);
                        $('#slPaymentMode').val(member2.policymode);
                    })
                }) 
            }
            else
            {
                $('.closeGroup').css({'display' : 'none'});
            }
            
            if (member2.label == "Recruit")
            {
                $('.rec').css({'display' : 'block'});
                $('#txtPotentialNewRecruit').val(member2.recruit);
                $('#txtNewRecruitPhone').val(member2.recruitphone);
            }
            else
            {
                $('.rec').css({'display' : 'none'});
            }
            
            var tt = member2.activitydatetime.split(' ')[0].split('-');
            $('#txtDate').val(tt[2] + '/' + tt[1] + '/' + tt[0]);  
            $('#slActions').val(member2.label);
            $('#txtLat').val(member2.latitude);
            $('#txtLng').val(member2.longitude);
            $('#txtRemark').val(member2.remarks);
            
            if (member2.photo != '')
            {
                $('#divUpload').hide(100); 
                $('#divImage').show(100);
                document.getElementById("imgUpload").src = member2.photo;
            }
            $('#btn-save').hide(100);
            $('#modalAction').modal('show');
            })
        })  
    });
    
    $('.policy').click(function(event){
        
        var member_id = event.target.id.replace("policy","");
        var url = "/getPolicy/" + member_id;        
        
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
                $('#modalPolicyLabel').html("Update Policy: " + member2.policyid);
                $('#policy_id').val(member2.id);
                $('#txtPolicyNumberPolicy').val(member2.policyid);
                $('#txtInsuredNamePolicy').val(member2.insuredname);
                $('#txtANPAmountPolicy').val(member2.anp);
                $('#txtFYPAmountPolicy').val(member2.fyp);
                $('#txtStartDatePolicy').val(member2.startdate);
                $('#slPaymentFrequencyPolicy').val(member2.policyterm);
                $('#slPaymentModePolicy').val(member2.policymode);

                $('#modalPolicy').modal('show');
            })
        })  
    });
    
    $('#btn-savePolicy').click(function()
        {
           $('#divPolicyNumberPolicy').html('');
           $('#divInsuredNamePolicy').html('');
           $('#divANPAmountPolicy').html('');
           $('#divFYPAmountPolicy').html('');
           $('#divStartDatePolicy').html('');
           $('#divPaymentFrequencyPolicy').html('');
           $('#divPaymentModePolicy').html('');
           $('#statusMsgPolicy').html('');
           
           $.ajax({
                   type: 'POST',
                   url: '/updatePolicy',
                   dataType: "json",
                   data: {
                       insuredname: $('#txtInsuredNamePolicy').val(),
                       anp: $('#txtANPAmountPolicy').val(),
                       fyp: $('#txtFYPAmountPolicy').val(),
                       paymentmode: $('#slPaymentModePolicy').val(),
                       paymentfrequency: $('#slPaymentFrequencyPolicy').val(),
                       policystart: $('#txtStartDatePolicy').val(),
                       policyid: $('#txtPolicyNumberPolicy').val(),
                       id: $('#policy_id').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#divPolicyNumberPolicy').html('');
                           $('#divInsuredNamePolicy').html('');
                           $('#divANPAmountPolicy').html('');
                           $('#divFYPAmountPolicy').html('');
                           $('#divStartDatePolicy').html('');
                           $('#divPaymentFrequencyPolicy').html('');
                           $('#divPaymentModePolicy').html('');
                           $('#statusMsgPolicy').attr("class", "alert alert-info");
                           $('#statusMsgPolicy').html(data.message);
                       }
                       else
                       {
                           $('#statusMsgPolicy').attr("class", "alert alert-danger");
                           $('#statusMsgPolicy').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsgPolicy').attr("class", "alert alert-danger");
                       $('#statusMsgPolicy').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'insuredname')
                           {
                               $('#divInsuredNamePolicy').html(member2);
                           }
                           if (elem == 'anp')
                           {
                               $('#divANPAmountPolicy').html(member2);
                           }
                           if (elem == 'fyp')
                           {
                               $('#divFYPAmountPolicy').html(member2);
                           }
                           if (elem == 'policystart')
                           {
                               $('#divStartDatePolicy').html(member2);
                           }
                           if (elem == 'paymentmode')
                           {
                               $('#divPaymentModePolicy').html(member2);
                           }
                           if (elem == 'paymentfrequency')
                           {
                               $('#divPaymentFrequencyPolicy').html(member2);
                           }
                        })                       
                   }
               });
       });
    
    
    $('#modalAction').on('shown.bs.modal', function () {
        var options = {
            componentRestrictions: {country: "my"}
        };
        input = document.getElementById('txtLocationSearch');
        initMap();
        autoc = new google.maps.places.Autocomplete(input, options);
        
        
        google.maps.event.trigger(map, "resize");
        
        marker = new google.maps.Marker(
            {
                position: {lat: parseFloat($('#txtLat').val()), lng: parseFloat($('#txtLng').val())},
                map: map,
                draggable: true
        
            }           
            );
    
    
        autoc.addListener('place_changed', function()
        {
            var place = autoc.getPlace();
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            
            map.fitBounds(bounds);
            map.setZoom(15);
        });
        
        google.maps.event.addListener(marker, 'position_changed', function()
        {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#txtLat').val(lat);
            $('#txtLng').val(lng);
        });
        
        document.getElementById("fuImage").addEventListener("change", readFile);
        
    });
    
    $('#btnDeletePhoto').click(function(event)
    { 
        resetImage();
        
    });
});

var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    center: { lat: parseFloat($('#txtLat').val()), lng: parseFloat($('#txtLng').val()) },
    zoom: 15
  })
}

function resetImage()
{
    document.getElementById("imgUpload").src = "";
    document.getElementById("divHidden").value = "";

    $('#divUpload').show(100); 
    $('#divImage').hide(100);
    ('#fuImage').val("");
}

function showBrowseDialog() 
{
    var fileuploadctrl = document.getElementById('fuImage');
    fileuploadctrl.click();
}

function readFile()
{
    console.log($('#fuImage').val());
    $('#divUpload').hide(100); 
    $('#divImage').show(100);
    if (this.files && this.files[0]) {    
        var FR= new FileReader();
        FR.addEventListener("load", function(e) {
            document.getElementById("imgUpload").src       = e.target.result;
            document.getElementById("divHidden").value = e.target.result;
            document.getElementById("divHidden").value
            = document.getElementById("divHidden").value.split(",")[1];
        }); 
        FR.readAsDataURL(this.files[0]);
    }
}
