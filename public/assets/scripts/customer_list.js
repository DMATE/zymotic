/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {  
        $jq1( function() {
        $jq1( "#txtDate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
          }).datepicker("setDate", "0");
          
        $jq1( "#txtStartDate" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
          }).datepicker("setDate", "0");
        } );
        
        $('#btn-saveEdit').click(function()
        {
           $('#divFullNameEdit').html('');
           $('#divEmailEdit').html('');
           $('#divPhoneNumberEdit').html('');
           $('#divAgentEdit').html('');
           $('#statusMsg').html('');
           
           $.ajax({
                   type: 'POST',
                   url: '/updateCustomer',
                   dataType: "json",
                   data: {
                       name: $('input[name="txtFullNameEdit"]').val(),
                       phonenumber: $('input[name="txtPhoneNumberEdit"]').val(),
                       phoneprefix: $('#slPrefixEdit').val(),
                       agent: $('#txtAgentEdit').val(),
                       address: $('#txtAddressEdit').val(),
                       id: $('input[name="txtIDEdit"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#divFullNameEdit').html();
                           $('#divPhoneNumberEdit').html();
                           $('#divAgentEdit').html();
                           $('#statusMsgEdit').attr("class", "alert alert-info");
                           $('#statusMsgEdit').html(data.message);
                       }
                       else
                       {
                           $('#statusMsgEdit').attr("class", "alert alert-danger");
                           $('#statusMsgEdit').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsgEdit').attr("class", "alert alert-danger");
                       $('#statusMsgEdit').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'name')
                           {
                               $('#divFullNameEdit').html(member2);
                           }
                           if (elem == 'phonenumber')
                           {
                               $('#divPhoneNumberEdit').html(member2);
                           }
                           if (elem == 'agent')
                           {
                               $('#divAgentEdit').html(member2);
                           }
                        })                       
                   }
               });
       });
       
       $('#btn-save').click(function()
        {
           $('#divDate').html('');
           $('#statusMsg').html('');
           $('#divANPAmount').html('');
            $('#divStartDate').html('');
            $('#divInsuredName').html('');
            $('#divPaymentMode').html('');
            $('#divPaymentFrequency').html('');
            $('#divNumberRef').html('');
            $('#divPotentialNewRecruit').html('');
            $('#divNewRecruitPhone').html('');
           
           if (($('#slActions')).val() == 'Visit')
           {
                $url = '/createVisit';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);                           
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                        })                       
                   }
               });
           } 
           else if (($('#slActions')).val() == 'Approach')
           {
               $url = '/createApproach';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       potentialanp: $('#txtPotentialANP').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'potentialanp')
                           {
                               $('#divPotentialANP').html(member2);
                           }
                        })                       
                   }
               });
           }
           else if (($('#slActions')).val() == 'Deal')
           {
               $url = '/createDeal';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       anp: $('#txtANPAmount').val(),
                       fyp: $('#txtFYPAmount').val(),
                       paymentmode: $('#slPaymentMode').val(),
                       paymentfrequency: $('#slPaymentFrequency').val(),
                       insuredname: $('#txtInsuredName').val(),
                       policystart: $('#txtStartDate').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'anp')
                           {
                               $('#divANPAmount').html(member2);
                           }
                           else if (elem == 'fyp')
                           {
                               $('#divFYPAmount').html(member2);
                           }
                           else if (elem == 'policystart')
                           {
                               $('#divStartDate').html(member2);
                           }
                           else if (elem == 'insuredname')
                           {
                               $('#divInsuredName').html(member2);
                           }
                           else if (elem == 'paymentmode')
                           {
                               $('#divPaymentMode').html(member2);
                           }
                           else if (elem == 'paymentfrequency')
                           {
                               $('#divPaymentFrequency').html(member2);
                           }
                        })                       
                   }
               });
           }
           else if (($('#slActions')).val() == 'Referral')
           {
               $url = '/createReferral';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       numref: $('#txtNumberRef').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'numref')
                           {
                               $('#divNumberRef').html(member2);
                           }
                        })                       
                   }
               });
           }
           else if (($('#slActions')).val() == 'Recruit')
           {
               $url = '/createRecruit';
                $.ajax({
                   type: 'POST',
                   url: $url,
                   dataType: "json",
                   data: {
                       label: $('#slActions').val(),
                       datetimevisit: $('input[name="txtDate"]').val(),
                       remarks: $('input[name="txtRemark"]').val(),
                       id: $('input[name="action_cust_id"]').val(),
                       lat: $('#txtLat').val(),
                       lng: $('#txtLng').val(),
                       imgstr: $('#divHidden').val(),
                       recname: $('#txtPotentialNewRecruit').val(),
                       recphone: $('#txtNewRecruitPhone').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       if (data.success == "true")
                       {
                           $('#statusMsg').attr("class", "alert alert-info");
                           $('#statusMsg').html(data.message);
                           resetImage();
                       }
                       else
                       {
                           $('#statusMsg').attr("class", "alert alert-danger");
                           $('#statusMsg').html(data.message);
                       }
                   },
                   error: function(xhr)
                   {
                       $('#statusMsg').attr("class", "alert alert-danger");
                       $('#statusMsg').html("Please correct the following error(s).");
                       console.log(xhr);
                       $.each(JSON.parse(xhr.responseText), function(elem, member2){
                           if (elem == 'datetimevisit')
                           {
                               $('#divDate').html(member2);
                           }
                           else if (elem == 'recname')
                           {
                               $('#divPotentialNewRecruit').html(member2);
                           }
                           else if (elem == 'recphone')
                           {
                               $('#divNewRecruitPhone').html(member2);
                           }
                        })                       
                   }
               });
           }
        });
       
       $jq1("#txtAgent").autocomplete({
           source: function(request, response) {
               $.ajax({
                   type: 'POST',
                   url: '/searchAgent',
                   dataType: "json",
                   data: {
                       term : request.term,
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                     response($.map(data, function (item) {
                                return item.split('|')[0] + " - " +
                                        item.split('|')[1] + " - " + 
                                        item.split('|')[2];
                            }))
                   }
               });
           },
           min_length: 1
       });
    
        $jq1("#txtFullName").autocomplete({
           source: function(request, response) {
               $.ajax({
                   type: 'POST',
                   url: '/searchProspectName',
                   dataType: "json",
                   data: {
                       term : request.term,
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                     response($.map(data, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1],
                                    em: item.split('|')[2]
                                }
                            }))
                   }
               });
           },
           min_length: 1
       });
       
       $('#aLinkAdvanced').click(function()
       {
           if ($('#divAdvanced').css('display') == 'none')
           {
               $('#divAdvanced').css({'display' : 'block'});
               $('#aLinkAdvanced').text('Basic Search');
           }
           else
           {
               $('#divAdvanced').css({'display' : 'none'});
               $('#aLinkAdvanced').text('Advanced Search');
           }
       });
       
       
       $('#txtFullName').on('input', function() {
            // do your stuff
            $.ajax({
                   type: 'POST',
                   url: '/searchProspectList',
                   dataType: "json",
                   data: {
                       term : $('input[name="txtFullName"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       displayProspects(data);
                   }
               });
        });
        
        $("#slActions").change(function() {
            if (this.value == "Approach")
            {
                $('#fgPotentialANP').css({'display' : 'block'});
            }
            else
            {
                $('#fgPotentialANP').css({'display' : 'none'});
            }
            
            if (this.value == "Referral")
            {
                $('#fgNumberRef').css({'display' : 'block'});
            }
            else
            {
                $('#fgNumberRef').css({'display' : 'none'});
            }
            
            if (this.value == "Deal")
            {
                $('.closeGroup').css({'display' : 'block'});
            }
            else
            {
                $('.closeGroup').css({'display' : 'none'});
            }
            
            if (this.value == "Recruit")
            {
                $('.rec').css({'display' : 'block'});
            }
            else
            {
                $('.rec').css({'display' : 'none'});
            }
            
        });
        
        
        $("#btnSearch").click( function()
        {
           
           $.ajax({
                   type: 'POST',
                   url: '/searchProspects',
                   dataType: "json",
                   data: {
                       name: $('input[name="txtFullName"]').val(),
                       agent: $('input[name="txtAgent"]').val(),
                       phonenumber: $('input[name="txtPhoneNumber"]').val(),
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                       console.log(data);
                       displayProspects(data);
                       
                   }
               });
        });
});

var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 3.139, lng: 101.6869},
    zoom: 15
  })
}

function displayProspects(data)
{
    var tbody = $("#tblProspects tbody");
    tbody.empty();
    var tag_start = "<td>";
    var tag_end = "</td>";
    
    $.each(data.members, function(elem, member){
            var tr = $("<tr>");
            var fontstart = "<span style='color: red'>";
            var fontend = "</span>";
            //var n = member.name.indexOf($('input[name="txtFullName"]').val());
            var len = fontstart + $('input[name="txtFullName"]').val() + fontend;
            var name = member.name.replace($('input[name="txtFullName"]').val(), len);
            tr.append(tag_start + member.id + tag_end);
            tr.append(tag_start + name + tag_end);
            tr.append(tag_start + member.phoneprefix + '-' + member.phone + tag_end);
            tr.append(tag_start + member.agent.name + tag_end);
            tr.append("<td><input type='button' value='Edit' class='btn btn-warning edit' id=edit" + member.id + ">\
    &nbsp;<input type='button' value='Actions' class='btn btn-success actions' id=action" + member.id + ">&nbsp;<input type='button' value='View' class='btn btn-primary view' id=view" + member.id + "></td>");
            tbody.append(tr);
        })
        
        
    $('.actions').click(function(event){
        
        var member_id = event.target.id.replace("action","");
        var url = "/getCustomer/" + member_id;        
       
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
            $('#modalActionLabel').html("Actions: " + member2.name);
            $('#action_cust_id').val(member2.id);
            $('#modalAction').modal('show');
            })
        })  
    });
    
    $('.view').click(function(event){        
        var member_id = event.target.id.replace("view","");
        window.location.href = "/view_customer/" + member_id; 
    });
    
    $('#btnDeletePhoto').click(function(event)
    { 
        resetImage();
        
    });
    
    $('.edit').click(function(event){
        
        var member_id = event.target.id.replace("edit","");
        var url = "/getCustomer/" + member_id;        
       
        $.get(url, function (data2) {
            $.each(data2.member, function(elem, member2){
            $('#modalEditLabel').html("Edit: " + member2.name);
            //$('#task_id').val(member2.id);
            
            $('#txtIDEdit').val(member2.id);
            $('#txtFullNameEdit').val(member2.name);
            $('#slPrefixEdit').val(member2.phoneprefix);
            $('#txtPhoneNumberEdit').val(member2.phone);
            $('#txtAddressEdit').val(member2.address);
            $('#txtAgentEdit').val(member2.agent.name + ' - ' + member2.agent.agentid + ' - ' + member2.agent.id);
            
            $('#modalEdit').modal('show');
            })
        })  
    });
    
    
    
    $('#modalAction').on('shown.bs.modal', function () {
        
        var options = {
            componentRestrictions: {country: "my"}
        };
        input = document.getElementById('txtLocationSearch');
        initMap();
        autoc = new google.maps.places.Autocomplete(input, options);
        
        
        google.maps.event.trigger(map, "resize");
        
        marker = new google.maps.Marker(
            {
                position: {lat: 3.139, lng: 101.6869},
                map: map,
                draggable: true
        
            }           
            );
    
        $('#txtLat').val("3.139");
        $('#txtLng').val("101.6869");
    
        autoc.addListener('place_changed', function()
        {
            var place = autoc.getPlace();
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            
            map.fitBounds(bounds);
            map.setZoom(15);
        });
        
        google.maps.event.addListener(marker, 'position_changed', function()
        {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#txtLat').val(lat);
            $('#txtLng').val(lng);
        });
        
        document.getElementById("fuImage").addEventListener("change", readFile);
        
    })
    
}



function resetImage()
{
    document.getElementById("imgUpload").src = "";
    document.getElementById("divHidden").value = "";

    $('#divUpload').show(100); 
    $('#divImage').hide(100);
    ('#fuImage').val("");
}

function showBrowseDialog() 
{
    var fileuploadctrl = document.getElementById('fuImage');
    fileuploadctrl.click();
}

function readFile()
{
    console.log($('#fuImage').val());
    $('#divUpload').hide(100); 
    $('#divImage').show(100);
    if (this.files && this.files[0]) {    
        var FR= new FileReader();
        FR.addEventListener("load", function(e) {
            document.getElementById("imgUpload").src       = e.target.result;
            document.getElementById("divHidden").value = e.target.result;
            document.getElementById("divHidden").value
            = document.getElementById("divHidden").value.split(",")[1];
        }); 
        FR.readAsDataURL(this.files[0]);
    }
}




