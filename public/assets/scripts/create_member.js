/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {    
        $jq1("#txtUpline").autocomplete({
           source: function(request, response) {
               $.ajax({
                   type: 'POST',
                   url: '/searchByNameId',
                   dataType: "json",
                   data: {
                       term : request.term,
                       _token : $('input[name="_token"]').val()
                   },
                   success: function(data) {
                     response($.map(data, function (item) {
                                return item.split('|')[0] + " - " +
                                        item.split('|')[1];
                            }))
                   }
               });
           },
           min_length: 1
       });
});

