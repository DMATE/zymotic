<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'password' => array('required', 'min:6',
                              'regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/'),
            'cpassword' => 'required|same:password',
        ];
    }
    
    public function messages()
        {
             return [
                'password.required' => 'Please enter password',
                'password.min' => 'Pasword shall be at least 6 characters',
                'password.regex' => 'Password shall be Combination of number and alphabet',
                'cpassword.required' => 'Please enter confirm password',
                'cpassword.same' => 'Password and confirm password not matched'
             ];
        }
    
    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            return response()->json($errors, 422);
        }

        return response()->json($errors);
    }
}
