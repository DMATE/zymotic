<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdatePolicyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'insuredname' => 'required',
            'anp' => 'required|numeric',
            'fyp' => 'required|numeric',
            'paymentmode' => 'required',
            'paymentfrequency' => 'required',
            'policystart' => 'required'
        ];
    }
    
    public function messages()
    {
         return [
             'insuredname.required' => 'Please enter insured name.',
             'anp.required' => 'Please enter anp.',
             'anp.numeric' => 'Please enter anp. Only numeric allowed.',
             'fyp.required' => 'Please enter fyp.',
             'fyp.numeric' => 'Please enter fyp. Only numeric allowed.',
             'paymentmode.required' => 'Please select payment mode.',
             'paymentfrequency.required' => 'Please select payment frequency.',
             'policystart.required' => 'Please enter policy start date.',
         ];
    }
    
    public function response(array $errors)
    {
        if ($this->ajax()||$this->isJson())
        {
            return response()->json($errors, 422);
        }

        return response()->json($errors);
    }
}
