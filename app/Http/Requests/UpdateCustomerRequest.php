<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|min:3|max:255',
            'phonenumber' => array('required', 'min:7', 'max:9', 'regex:/[0-9]+/'),
            'agent' => 'required'
        ];
    }
    
    public function messages()
    {
         return [
             'name.required' => 'Please enter full name.',
             'name.min' => 'Full name must more than 3 characters',
             'name.max' => 'Full name must not more than 255 characters',
             'phonenumber.required' => 'Please enter phone number',
             'phonenumber.min' => 'Please enter valid phone number',
             'phonenumber.max' => 'Please enter valid phone number',
             'phonenumber.regex' => 'Please enter valid phone number',
             'agent.required' => 'Please enter agent'
         ];
    }
    
    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            return response()->json($errors, 422);
        }

        return response()->json($errors);
    }
}
