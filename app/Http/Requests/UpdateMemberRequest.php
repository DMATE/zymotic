<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateMemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|min:3|max:255',
            'email' => 'required|email',
            'phonenumber' => array('required', 'min:7', 'max:9', 'regex:/[0-9]+/'),
            'agentid' => 'required'
        ];
    }
    
    public function messages()
        {
             return [
                 'name.required' => 'Please enter full name.',
                 'name.min' => 'Full name must more than 3 characters',
                 'name.max' => 'Full name must not more than 255 characters',
                 'email.required' => 'Please enter email address',
                 'email.email' => 'Email is not in valid format',
                 'phonenumber.required' => 'Please enter phone number',
                 'phonenumber.min' => 'Please enter valid phone number',
                 'phonenumber.max' => 'Please enter valid phone number',
                 'phonenumber.regex' => 'Please enter valid phone number',
                 'agentid.required' => 'Please enter agent id'
             ];
        }
    
    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            return response()->json($errors, 422);
        }

        return response()->json($errors);
    }
}
