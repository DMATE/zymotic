<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateActivityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'label' => 'required',
            'datetimevisit' => 'required',
            'potentialanp' => 'sometimes|required|numeric',
            'insuredname' => 'sometimes|required',
            'anp' => 'sometimes|required|numeric',
            'fyp' => 'sometimes|required|numeric',
            'paymentmode' => 'sometimes|required',
            'paymentfrequency' => 'sometimes|required',
            'policystart' => 'sometimes|required',
            'numref' => 'sometimes|required|integer',
            'recname' => 'sometimes|required',
            'recphone' => 'sometimes|required'
        ];
    }
    
    public function messages()
    {
         return [
             'label.required' => 'Please enter activity.',
             'datetimevisit.required' => 'Please enter activity date.',
             'potentialanp.required' => 'Please enter potential anp.',
             'potentialanp.numeric' => 'Please enter valid anp. Only numeric allowed.',
             'insuredname.required' => 'Please enter insured name.',
             'anp.required' => 'Please enter anp.',
             'anp.numeric' => 'Please enter anp. Only numeric allowed.',
             'fyp.required' => 'Please enter fyp.',
             'fyp.numeric' => 'Please enter fyp. Only numeric allowed.',
             'paymentmode.required' => 'Please select payment mode.',
             'paymentfrequency.required' => 'Please select payment frequency.',
             'policystart.required' => 'Please enter policy start date.',
             'numref.required' => 'Please enter number of referral.',
             'numref.integer' => 'Please enter valid number of referral. Only numeric allowed.',
             'recname.required' => 'Please enter recruit name.',
             'recphone.required' => 'Please enter recruit phone.',
         ];
    }
    
    public function response(array $errors)
    {
        if ($this->ajax() || $this->wantsJson())
        {
            return response()->json($errors, 422);
        }

        return response()->json($errors);
    }
}
