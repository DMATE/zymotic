<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterCustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'txtFullName' => 'required|min:3|max:255',
            'txtPhoneNumber' => array('required', 'min:7', 'max:9', 'regex:/[0-9]+/'),
            'txtAgent' => 'required'
        ];
    }
    
    public function messages()
    {
         return [
             'txtFullName.required' => 'Please enter full name.',
             'txtFullName.min' => 'Full name must more than 3 characters',
             'txtFullName.max' => 'Full name must not more than 255 characters',
             'txtPhoneNumber.required' => 'Please enter phone number',
             'txtPhoneNumber.min' => 'Please enter valid phone number',
             'txtPhoneNumber.max' => 'Please enter valid phone number',
             'txtPhoneNumber.regex' => 'Please enter valid phone number',
             'txtAgent.required' => 'Please enter agent'
         ];
    }
}
