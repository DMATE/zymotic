<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterAdminRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
                    'txtFullName' => 'required|min:3|max:255',
                    'txtEmail' => 'required|email',
                    'txtPassword' => array('required', 'min:6',
                                      'regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/'),
                    'txtCPassword' => 'required|same:txtPassword'
		];
	}
        
        public function messages()
        {
             return [
                 'txtFullName.required' => 'Please enter full name.',
                 'txtFullName.min' => 'Full name must more than 3 characters',
                 'txtFullName.max' => 'Full name must not more than 255 characters',
                 'txtEmail.required' => 'Please enter email address',
                 'txtEmail.email' => 'Email is not in valid format',
                 'txtPassword.required' => 'Please enter password',
                 'txtPassword.min' => 'Pasword shall be at least 6 characters',
                 'txtPassword.regex' => 'Password shall be Combination of number and alphabet',
                 'txtCPassword.required' => 'Please enter confirm password',
                 'txtCPassword.same' => 'Password and confirm password not matched'
             ];
        }

}
