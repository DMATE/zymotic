<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
use Illuminate\Http\Request;

Route::get('/charts', function()
{
	return View::make('mcharts');
});

Route::get('/tables', function()
{
	return View::make('table');
});

Route::get('/forms', function()
{
	return View::make('form');
});

Route::get('/grid', function()
{
	return View::make('grid');
});

Route::get('/buttons', function()
{
	return View::make('buttons');
});


Route::get('/icons', function()
{
	return View::make('icons');
});

Route::get('/panels', function()
{
	return View::make('panel');
});

Route::get('/typography', function()
{
	return View::make('typography');
});

Route::get('/notifications', function()
{
	return View::make('notifications');
});

Route::get('/blank', function()
{
	return View::make('blank');
});
      
Route::get('/documentation', function()
{
	return View::make('documentation');
});


Route::post('authenticate', function()
{
    $uname = Input::get('email');
    $pwd = Input::get('password');
    if (\Auth::attempt("admin", ['email' => $uname, 'password' => $pwd]))
    {
        Session::put('uname', $uname);
        return redirect()->route('home')->with('flash_pass', 'Login Successful.');
    }
    else {
        Session::forget('uname');
        return redirect()->route('login')->with('flash_error', 'Invalid Login. Incorrect Email or Password.');
    }    
});


/*** This is added to test Sarav Multi Auth **/
// Users route
Route::controller('/user', 'Auth\AuthController');
Route::controller('/password', 'Auth\PasswordController');
 
// Admin route
Route::controller('/admin', 'Auth\AdminController');
Route::controller('/admin/password', 'Auth\AdminPasswordController');
Route::get('/register_admin', ['as' => 'register_admin', 'uses' => 'AdminController@register_admin']);
Route::get('/register_member', ['as' => 'register_member', 'uses' => 'AdminController@register_member']);
Route::get('/view_member', ['as' => 'view_member', 'uses' => 'AdminController@view_member']);
Route::get('/', ['as' => 'home', 'uses' => 'AdminController@getHome']);
Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@admin']); 
Route::post('/create_admin', ['as' => 'create_admin', 'uses' => 'AdminController@create_admin']);
Route::post('/searchMember', ['as' => 'searchMember', 'uses' => 'AdminController@search_member']);
Route::get('/getMember/{id}', ['as' => 'getMember', 'uses' => 'AdminController@get_member']);
Route::post('/create_member', ['as' => 'create_member', 'uses' => 'AdminController@create_member']);
Route::post('/searchByName', ['as' => 'search_name', 'uses' => 'AdminController@searchByName']);
Route::post('/searchByNameId', ['as' => 'search_name_id', 'uses' => 'AdminController@searchByNameWithID']);
Route::post('/updateAgent', ['as' => 'update_agent', 'uses' => 'AdminController@updateAgent']);
Route::post('/updateAgentPassword', ['as' => 'update_agent_password', 'uses' => 'AdminController@updateAgentPassword']);
Route::get('/getTree/{id}', ['as' => 'get_tree', 'uses' => 'AdminController@treeView']);
//Route::get('searchajax',array('as'=>'searchajax','uses'=>'AdminController@autoComplete'));
Route::get('/register_customer', ['as' => 'register_customer', 'uses' => 'AdminController@register_customer']);
Route::post('/searchAgent', ['as' => 'search_agent', 'uses' => 'AdminController@searchAgent']);
Route::post('/create_customer', ['as' => 'create_customer', 'uses' => 'AdminController@create_customer']);
Route::get('/customer_list', ['as' => 'customer_list', 'uses' => 'AdminController@customer_list']);
Route::post('/searchProspectName', ['as' => 'searchProspectName', 'uses' => 'AdminController@searchProspectName']);
Route::post('/searchProspectList', ['as' => 'searchProspectList', 'uses' => 'AdminController@searchProspectList']);
Route::get('/getCustomer/{id}', ['as' => 'getCustomer', 'uses' => 'AdminController@get_customer']);
Route::get('/getPolicy/{id}', ['as' => 'getPolicy', 'uses' => 'AdminController@get_policy']);
Route::get('/getPolicyByActivity/{id}', ['as' => 'getPolicyByActivity', 'uses' => 'AdminController@get_policybyactivity']);
Route::get('/getActivity/{id}', ['as' => 'getActivity', 'uses' => 'AdminController@get_activity']);
Route::post('/searchProspects', ['as' => 'searchProspects', 'uses' => 'AdminController@searchProspects']);
Route::post('/updateCustomer', ['as' => 'update_customer', 'uses' => 'AdminController@update_customer']);
Route::post('/updatePolicy', ['as' => 'update_policy', 'uses' => 'AdminController@update_policy']);
Route::post('/createVisit', ['as' => 'visitCustomer', 'uses' => 'AdminController@visitCustomer']);
Route::post('/createApproach', ['as' => 'approachCustomer', 'uses' => 'AdminController@approachCustomer']);
Route::post('/createDeal', ['as' => 'dealCustomer', 'uses' => 'AdminController@dealCustomer']);
Route::post('/createReferral', ['as' => 'referCustomer', 'uses' => 'AdminController@referCustomer']);
Route::post('/createRecruit', ['as' => 'recruitCustomer', 'uses' => 'AdminController@recruitCustomer']);
Route::get('/view_customer/{id}', ['as' => 'view_customer', 'uses' => 'AdminController@view_customer']);

//-- Web API
Route::get('/api/v1/test', ['middleware' => 'auth.basic', function() {    
    return Response::json(array(
                'status' => 'pass',
                'message' => 'successfully connected',
                'status_code' => 200
            ));
}]);

// -- Web Web API
Route::post('/oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

Route::get('hello', ['middleware' => 'oauth', function() {
    return 'hello world';
}]);

Route::get('/api/retrieveProfile/{id}', ['middleware' => 'oauth', 'as' => 'retrieveProfile', 'uses' => 'APIController@retrieveProfile']);
Route::get('/api/viewCustomer/{id}', ['middleware' => 'oauth', 'as' => 'view_customer', 'uses' => 'APIController@view_customer']);
Route::get('/api/getPolicyByActivity/{id}', ['middleware' => 'oauth', 'as' => 'getPolicyByActivity', 'uses' => 'APIController@get_policybyactivity']);
Route::post('/api/changePassword', ['middleware' => 'oauth', 'as' => 'changePassword', 'uses' => 'APIController@changePassword']);
Route::post('/api/createvisit', ['middleware' => 'oauth', 'as' => 'visitCustomer', 'uses' => 'APIController@visitCustomer']);
Route::post('/api/searchCustomers', ['middleware' => 'oauth', 'as' => 'retrieveCustomers', 'uses' => 'APIController@retrieveCustomers']);
Route::post('/api/retrieveactivities', ['middleware' => 'oauth', 'as' => 'retrieveActivityList', 'uses' => 'APIController@retrieveActivityList']);
Route::post('/api/retrieveallactivities', ['middleware' => 'oauth', 'as' => 'retriveAllActivityList', 'uses' => 'APIController@retriveAllActivityList']);
Route::post('/api/createapproach', ['middleware' => 'oauth', 'as' => 'approachCustomer', 'uses' => 'APIController@approachCustomer']);
Route::post('/api/createdeal', ['middleware' => 'oauth', 'as' => 'dealCustomer', 'uses' => 'APIController@dealCustomer']);
Route::post('/api/createreferral', ['middleware' => 'oauth', 'as' => 'referCustomer', 'uses' => 'APIController@referCustomer']);
Route::post('/api/createrecruit', ['middleware' => 'oauth', 'as' => 'recruitCustomer', 'uses' => 'APIController@recruitCustomer']);
Route::post('/api/updatepolicy', ['middleware' => 'oauth', 'as' => 'updatepolicy', 'uses' => 'APIController@update_policy']);


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => 'api.auth'] , function ($api) {
    $api->get('time', function () {
        return ['now' => microtime(), 'date' => date('Y-M-D',time())];
    });
});



