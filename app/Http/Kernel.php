<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',		
                \LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware::class,
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
                'admin'       => \App\Http\Middleware\AdminAuthenticate::class,
                'admin.guest' => \App\Http\Middleware\AdminGuest::class,
		'auth' => 'App\Http\Middleware\Authenticate',
                'auth.admin' => 'App\Http\Middleware\AdminAuthenticate', // newly added
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
                'oauth' => \LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware::class,
                'oauth-user' => \LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware::class,
                'oauth-client' => \LucaDegasperi\OAuth2Server\Middleware\OAuthClientOwnerMiddleware::class,
                'check-authorization-params' => \LucaDegasperi\OAuth2Server\Middleware\CheckAuthCodeRequestMiddleware::class,
                'csrf' => App\Http\Middleware\VerifyCsrfToken::class
	];

}
