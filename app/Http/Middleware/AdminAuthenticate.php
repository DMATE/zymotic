<?php


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
/**
 * Description of AdminAuthenticate
 *
 * @author DELL
 */
class AdminAuthenticate {
    //put your code here
    protected $auth;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   
    public function __construct(Guard $auth)
    {
        //dd($auth -> with('admin'));
        $this->auth = $auth->with('admin');
    }
        
    public function handle($request, Closure $next)
    {    
        if ($this->auth->guest('admin')) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('admin/login');
            }
        }
        return $next($request);
    }
}
