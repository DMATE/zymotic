<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
    
/**
 * Description of AdminGuest
 *
 * @author DELL
 */
class AdminGuest {
    //put your code here
    protected $auth;
    
    public function __construct(Guard $auth)
    {
        $this->auth = $auth->with('admin');
    }
    
    public function handle($request, Closure $next)
    {
        if ($this->auth->check('admin')) {
            return redirect('home');
        }
        
        return $next($request);
    }
}
