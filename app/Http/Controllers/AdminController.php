<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterAdminRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Encryption\Encrypter;
use App\Http\Controllers\Auth;
use Carbon\Carbon;

/**
 * Description of AdminController
 *
 * @author DELL
 */
class AdminController extends Controller {
    //put your code here
    
    public function __construct(Request $request)
    {
        if ($request->session()->has('uname')) {
            
        }
        else
        {
            return redirect()->route('login')->with('flash_error', 'You have to Login in order to perform as admin.') -> send();
        }
    }
            
    public function register_admin()
    {
        $roles = \DB::table('adminroles')
                    ->get(array('id', 'name'));
        //dd($roles);
        return \View('admin\register_admin', compact('roles', $roles));
    }
    
    public function create_admin(RegisterAdminRequest $prequest)
    {
        // When everything is ok
        try
        {
            $newAdmin = new \App\Admin();
            $newAdmin -> name = $prequest -> txtFullName;
            $newAdmin -> email = $prequest -> txtEmail;
            $newAdmin -> password = bcrypt($prequest -> txtPassword);
            
            $adminType = Input::has('optionsRadios') ? Input::get('optionsRadios') : null;            
            $newAdmin -> role = $adminType;
            $newAdmin -> save();
            
            Session::flash('message', "New admin registered successfully.");
            return Redirect::back();
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }        
    }  
    
    
    // <editor-fold defaultstate="collapsed" desc="Members Related Sections">
  
    public function create_member(\App\Http\Requests\RegisterMemberRequest $prequest)
    {
        // When everything is ok
        try
        {
            $upline_input = $prequest -> txtUpline;
            if (str_contains($upline_input, '-'))
            {  
                $myArray = explode(' - ', $upline_input);
                $str = trim($myArray[1]);                
                $upline_id = (int)$str;
            }
            else {
                $upline_id = 0;
            }
            $result = $prequest -> slPrefix;
            $newMember = new \App\User();
            $newMember -> name = $prequest -> txtFullName;
            $newMember -> email = $prequest -> txtEmail;
            $newMember -> password = bcrypt($prequest -> txtPassword);
            $newMember -> name = $prequest -> txtFullName;
            $newMember -> agentid = $prequest -> txtAgentId;  
            $newMember -> phoneprefix = $result; 
            $newMember -> phone = $prequest -> txtPhoneNumber; 
            $newMember -> uplineid = $upline_id;
            $newMember -> save();
                        
            Session::flash('message', "New member registered successfully.");
            return Redirect::back();
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }        
    } 
    
    public function updateAgent(\App\Http\Requests\UpdateMemberRequest $grequest)
    {
        try
        {
            $some = $grequest -> id;
            $newMember = \App\User::find($some);

            $upline_input = $grequest -> uplineid;
            if (str_contains($upline_input, '-'))
            {  
                $myArray = explode(' - ', $upline_input);
                $str = trim($myArray[1]);                
                $upline_id = (int)$str;
            }
            else {
                $upline_id = 0;
            }
            $result = $grequest -> phoneprefix;
            $newMember -> name = $grequest -> name;
            $newMember -> email = $grequest -> email;
            $newMember -> agentid = $grequest -> agentid;  
            $newMember -> phoneprefix = $result; 
            $newMember -> phone = $grequest -> phonenumber; 
            $newMember -> uplineid = $upline_id;
            $newMember -> save();
            return response()->json(['success' => 'true', 'message' => 'Agent updated successful.']);
        }
        catch (\Exception $e) {
            return response()->json(['success' => 'false', 'message' => $e-> getMessage()]); 
        } 
    }
    
    public function updateAgentPassword(\App\Http\Requests\UpdatePasswordRequest $grequest)
    {
        try
        {
            $some = $grequest -> id;
            $newMember = \App\User::find($some);

            
            $newMember -> password = bcrypt($grequest -> password); 
            $newMember -> save();
            return response()->json(['success' => 'true', 'message' => 'Agent password updated successful.']);
        }
        catch (\Exception $e) {
            return response()->json(['success' => 'false', 'message' => $e-> getMessage()]); 
        } 
    }
    
    public function searchByName(Request $request)
    {        
        $query = $request->get('term','');
        $something = \App\User::where('name', 'LIKE', '%'.$query.'%') -> get(); 
        
        $data=array();
        foreach ($something as $some) {
                $data[]=$some -> name.'|'.$some -> agentid.'|'.$some -> email;
        }
        
        //return ;response()->json($something)
        //$things =  [$query.'-pp', '200-4', '300-3', '400-2', '500-1'];;
        
        return response()->json($data);
    }
    
    public function searchByNameWithID(Request $request)
    {        
        $query = $request->get('term','');
        $something = \App\User::where('name', 'LIKE', '%'.$query.'%') -> get(); 
        
        $data=array();
        foreach ($something as $some) {
                $data[]=$some -> name.'|'.$some -> id.'|'.$some -> email;
        }
        
        //return ;response()->json($something)
        //$things =  [$query.'-pp', '200-4', '300-3', '400-2', '500-1'];;
        
        return response()->json($data);
    }
    
    public function autoComplete(Request $request) {
        return ['value'=>'No Result Found','id'=>''];
    }
    
    public function register_member()
    {        
        return \View('admin\register_member');
    }
    
    public function get_member($id)
    {
        //$something = \App\User::
         //       where('id', '=', $id) -> get();
        
        
        $something = \DB::table('users as u1')
            ->rightJoin('users as u2', 'u2.uplineid', '=', 'u1.id')
            ->where('u2.id', '=', $id)
            ->select('u2.*', 'u1.name as uplinename')
            ->get();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    
    public function search_member(Request $request)
    {       
        $name = $request -> get('name','');
        $aiaId = $request -> get('agentId', '');
        $email = $request -> get('email', '');
        if($name == '' && $aiaId == '' && $email == '')
        {            // all empty get all
            $something = \App\User::all();
        }
        else
        {
            $something = \App\User::
                where('name', 'LIKE', '%'.$name.'%')
                    ->where('email', 'LIKE', '%'.$email.'%')
                    ->where('agentId', 'LIKE', '%'.$aiaId.'%')
                    ->get();
        }
        return response()->json(['success' => true, 'members' => $something]);
    }
    
    public function view_member()
    {        
        return \View('admin\view_member');
    }
    
    public function getHome()
    {
        return \View('admin\home');
    }
    
    public function treeView($id){       
        $agents = \App\User::where('id', '=', $id)->get();
        $tree='<ul id="browser" class="filetree"><li class="treeview"></li>';
        
        $count = 0;
        
        foreach ($agents as $agent) {
            if ($count == 0)
            {
                $name = $agent->name;
                $count = $count + 1;
            }
             $tree .='<li class="treeview closed"<a class="treename">'.$agent->name.'</a>';
             if(count($agent->childs)) {
                $tree .=$this->childView($agent);
            }
        }
        $tree .='<ul>';
        // return $tree;
        return response()->json(['name' => $name, 'tree' => $tree]);
    } 
    
    public function childView($user){                 
            $html ='<ul>';
            foreach ($user->childs as $arr) {
                if(count($arr->childs)){
                $html .='<li class="treeview closed"><a class="treename">'.$arr->name.'</a>';                  
                        $html.= $this->childView($arr);
                    }else{
                        $html .='<li class="treeview"><a class="treename">'.$arr->name.'</a>';                                 
                        $html .="</li>";
                    }                                   
            }
            
            $html .="</ul>";
            return $html;
    }
    
    // </editor-fold>
        
    
    // --- Customers
    // <editor-fold defaultstate="collapsed" desc="Customers Related Sections">
  
    public function register_customer()
    {
        $terms = \DB::table('terms')
                    ->get(array('id', 'fullname'));
        //dd($roles);
        return \View('admin\register_customer', compact('terms', $terms));
    }
    
    public function searchAgent(Request $request)
    {        
        $query = $request->get('term','');
        $something = \App\User::where('name', 'LIKE', '%'.$query.'%')
                -> orwhere('agentid', 'LIKE', '%'.$query.'%') -> get(); 
        
        $data=array();
        foreach ($something as $some) {
                $data[]=$some -> name.'|'.$some -> agentid.'|'.$some -> id;
        }
        
        //return ;response()->json($something)
        //$things =  [$query.'-pp', '200-4', '300-3', '400-2', '500-1'];;
        
        return response()->json($data);
    }
    
    public function create_customer(\App\Http\Requests\RegisterCustomerRequest $prequest)
    {
        try
        {
            $result = $prequest -> slPrefix;
            $upline_input = $prequest -> txtAgent;
            if (str_contains($upline_input, '-'))
            {  
                $myArray = explode(' - ', $upline_input);
                $str = trim($myArray[2]);                
                $upline_id = (int)$str;
            }
            else {
                $upline_id = 0;
            }
            
            $newCustomer = new \App\Customer();
            $newCustomer -> name = $prequest -> txtFullName;
            $newCustomer -> phoneprefix = $result; 
            $newCustomer -> phone = $prequest -> txtPhoneNumber; 
            $newCustomer -> address = $prequest -> txtAddress; 
            $newCustomer -> agentid = $upline_id;            
            $newCustomer -> updatedby = $prequest->session()->has('uname');
            $newCustomer -> save();
            
            Session::flash('message', "New customer registered successfully.");
            return Redirect::back();
        }
        catch (\Exception $e) {
            return $e->getMessage();
        } 
    }
    
    public function customer_list()
    {
        //dd($roles);
        return \View('admin\customer_list');
    }
    
    public function searchProspectName(Request $request)
    {        
        $query = $request->get('term','');
        $something = \App\Customer::where('name', 'LIKE', '%'.$query.'%')
                -> get(); 
        
        $data=array();
        foreach ($something as $some) {
                $data[]=$some -> name.'|'.$some -> id.'|'.$some -> phoneprefix.'-'.$some -> phone;
        }
        
        //return ;response()->json($something)
        //$things =  [$query.'-pp', '200-4', '300-3', '400-2', '500-1'];;
        
        return response()->json($data);
    }
    
    public function searchProspectList(Request $request)
    {        
        $query = $request->get('term','');
        $something = \App\Customer::with('Agent') -> where('name', 'LIKE', '%'.$query.'%')
                -> get(); 
        return response()->json(['success' => true, 'members' => $something]);
    }
    
    public function get_customer($id)
    {
        $something = \App\Customer::with('Agent')
            ->where('id', '=', $id)
            ->get();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    public function get_policy($id)
    {
        $something = \App\Policy::
            where('id', '=', $id)
            ->get();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    public function get_policybyactivity($id)
    {
        $something = \App\Policy::
            where('activityid', '=', $id)
            ->get();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    public function get_activity($id)
    {
        $something = \App\CustomerActivity::
            where('id', '=', $id)
            ->get();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    public function update_policy(\App\Http\Requests\UpdatePolicyRequest $urequest)
    {        
        // for new & old customer
        $id = $urequest->get('id', '');
        $policyid = $urequest->get('policyid', '');
        $anp = $urequest->get('anp', '');
        $fyp = $urequest->get('fyp', '');
        $insuredname = $urequest->get('insuredname', '');
        $paymentmode = $urequest->get('paymentmode', '');
        $paymentfrequency = $urequest->get('paymentfrequency', '');
        $policystart = $urequest->get('policystart', '');
        
        try
        {    
            $newpolicy = \App\Policy::find($id);
            $newpolicy -> policyid = $policyid;
            $newpolicy -> startdate = $policystart;
            $newpolicy -> policyterm = $paymentfrequency;
            $newpolicy -> policymode = $paymentmode;
            $newpolicy -> insuredname = $insuredname;
            $newpolicy -> anp = $anp;
            $newpolicy -> fyp = $fyp;
            $newpolicy -> save();
                
            return response()->json(['success' => 'true', 'message' => 'Policy updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$something]);
        } 
    }
    
    public function searchProspects(Request $request)
    {        
        $name = $request -> get('name','');
        $agent = $request -> get('agent', '');
        $phonenumber = $request -> get('phonenumber', '');
        //$policynumber = $request -> get('policynumber', '');
        //$minpolicy = $request -> get('minpolicy ', '');
        //$maxpolicy = $request -> get('maxpolicy', '');
        if($name == ''
                && $agent == ''
                && $phonenumber == '')
        {            // all empty get all
            $something = \App\Customer::with('Agent') ->with('Policy') ->get();
        }
        else
        {
            $something = \App\Customer::
                with('Agent') ->
                with('Policy') ->
                whereHas('Agent', function($query) use($agent) {
                    $query->where('name', 'like', '%'.$agent.'%');
                })->
                where('name', 'LIKE', '%'.$name.'%')
                    ->where(\DB::raw('CONCAT_WS(" ", phoneprefix, phone)'), 'like', '%'.$phonenumber.'%')                    
                    ->get();
        }
        return response()->json(['success' => true, 'members' => $something]);
    }
    
    public function update_customer(\App\Http\Requests\UpdateCustomerRequest $prequest)
    {
        try
        {
            $some = $prequest -> id;
            $result = $prequest -> phoneprefix;
            $upline_input = $prequest -> agent;
            if (str_contains($upline_input, '-'))
            {  
                $myArray = explode(' - ', $upline_input);
                $str = trim($myArray[2]);                
                $upline_id = (int)$str;
            }
            else {
                $upline_id = 0;
            }
            
            $newCustomer = \App\Customer::find($some);
            $newCustomer -> name = $prequest -> name;
            $newCustomer -> phoneprefix = $result; 
            $newCustomer -> phone = $prequest -> phonenumber; 
            $newCustomer -> agentid = $upline_id;
            $newCustomer -> address = $prequest -> address;
            $newCustomer -> save();
            
            return response()->json(['success' => 'true', 'message' => 'Prospect updated successfully.']);
        }
        catch (\Exception $e) {
            return response()->json(['success' => 'false', 'message' => $e-> getMessage()]); 
        } 
    }
        
    public function visitCustomer(\App\Http\Requests\UpdateActivityRequest $urequest)
    {        
        // for new & old customer
        $customer_id = $urequest->get('id','');
        $date_time_visit = $urequest->get('datetimevisit', '');
        $latitude = $urequest->get('lat','');
        $longitude = $urequest->get('lng','');
        $image_str = $urequest->get('imgstr', '');
        $remarks = $urequest->get('remarks', '');
        
        $newDate = \Carbon\Carbon::createFromFormat('d/m/Y', $date_time_visit)->format('Y-m-d');
        
        $activity_name = 'Visit';
        try
        {         
            if ($image_str != "")
            {
                $image = base64_decode(str_replace(' ', '+', $image_str));   
            
                //$img = imagecreatefromstring($image);

                $mytime = \Carbon\Carbon::now()->format('YmdHis');
                $test_path = '\img\visit'.$mytime.'.jpg';
                $image_path = public_path().$test_path;
                //Image::make($image->getRealPath())->save($image_path); 
                file_put_contents($image_path, $image);


                header('Content-Type: bitmap; charset=utf-8');
                $file = fopen($image_path, 'w');
                fwrite($file, $image);
                fclose($file);
            }
            else                
            {
                $test_path = '';
            }
            
            $newvisit = new \App\CustomerActivity();
            $newvisit -> customerid = $customer_id;
            $newvisit -> label = $activity_name;
            $newvisit -> activitydatetime = $newDate;
            $newvisit -> latitude = $latitude;
            $newvisit -> longitude = $longitude;
            $newvisit -> photo = $test_path;
            $newvisit -> remarks = $remarks;
            $newvisit -> save();
            return response()->json(['success' => 'true', 'message' => 'Activity updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    
    public function approachCustomer(\App\Http\Requests\UpdateActivityRequest $urequest)
    {        
        // for new & old customer
        $customer_id = $urequest->get('id','');
        $date_time_visit = $urequest->get('datetimevisit', '');
        $latitude = $urequest->get('lat','');
        $longitude = $urequest->get('lng','');
        $image_str = $urequest->get('imgstr', '');
        $remarks = $urequest->get('remarks', '');
        $potentialanp = $urequest->get('potentialanp', '');
        
        $newDate = \Carbon\Carbon::createFromFormat('d/m/Y', $date_time_visit)->format('Y-m-d');
        
        $activity_name = 'Approach';
        try
        {    
            if ($image_str != "")
            {
                $image = base64_decode(str_replace(' ', '+', $image_str));   
            
                //$img = imagecreatefromstring($image);

                $mytime = \Carbon\Carbon::now()->format('YmdHis');
                $test_path = '\img\visit'.$mytime.'.jpg';
                $image_path = public_path().$test_path;
                //Image::make($image->getRealPath())->save($image_path); 
                file_put_contents($image_path, $image);


                header('Content-Type: bitmap; charset=utf-8');
                $file = fopen($image_path, 'w');
                fwrite($file, $image);
                fclose($file);
            }
            else                
            {
                $test_path = '';
            }
            
            
            $newvisit = new \App\CustomerActivity();
            $newvisit -> customerid = $customer_id;
            $newvisit -> label = $activity_name;
            $newvisit -> activitydatetime = $newDate;
            $newvisit -> latitude = $latitude;
            $newvisit -> longitude = $longitude;
            $newvisit -> photo = $test_path;
            $newvisit -> remarks = $remarks;
            $newvisit -> potentialanp = $potentialanp;
            $newvisit -> save();
            return response()->json(['success' => 'true', 'message' => 'Activity updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    
    public function dealCustomer(\App\Http\Requests\UpdateActivityRequest $urequest)
    {        
        // for new & old customer
        $customer_id = $urequest->get('id','');
        $date_time_visit = $urequest->get('datetimevisit', '');
        $latitude = $urequest->get('lat','');
        $longitude = $urequest->get('lng','');
        $image_str = $urequest->get('imgstr', '');
        $remarks = $urequest->get('remarks', '');
        $anp = $urequest->get('anp', '');
        $fyp = $urequest->get('fyp', '');
        $insuredname = $urequest->get('insuredname', '');
        $paymentmode = $urequest->get('paymentmode', '');
        $paymentfrequency = $urequest->get('paymentfrequency', '');
        $policystart = $urequest->get('policystart', '');
        
        $newDate = \Carbon\Carbon::createFromFormat('d/m/Y', $date_time_visit)->format('Y-m-d');
        
        $activity_name = 'Deal';
        try
        {    
            if ($image_str != "")
            {
                $image = base64_decode(str_replace(' ', '+', $image_str));   
            
                //$img = imagecreatefromstring($image);

                $mytime = \Carbon\Carbon::now()->format('YmdHis');
                $test_path = '\img\visit'.$mytime.'.jpg';
                $image_path = public_path().$test_path;
                //Image::make($image->getRealPath())->save($image_path); 
                file_put_contents($image_path, $image);


                header('Content-Type: bitmap; charset=utf-8');
                $file = fopen($image_path, 'w');
                fwrite($file, $image);
                fclose($file);
            }
            else                
            {
                $test_path = '';
            }
            
            $newvisit = new \App\CustomerActivity();
            $newvisit -> customerid = $customer_id;
            $newvisit -> label = $activity_name;
            $newvisit -> activitydatetime = $newDate;
            $newvisit -> latitude = $latitude;
            $newvisit -> longitude = $longitude;
            $newvisit -> photo = $test_path;
            $newvisit -> remarks = $remarks;            
            $newvisit -> save();
            
            $something = \App\Customer::where('id', '=', $customer_id)->first();
            
            $newpolicy = new \App\Policy();
            $newpolicy -> customer = $customer_id;
            $newpolicy -> agent = $something->agentid;
            $newpolicy -> policyremark = $remarks;
            $newpolicy -> startdate = $policystart;
            $newpolicy -> policyterm = $paymentfrequency;
            $newpolicy -> policymode = $paymentmode;
            $newpolicy -> insuredname = $insuredname;
            $newpolicy -> anp = $anp;
            $newpolicy -> fyp = $fyp;
            $newpolicy -> updatedby = $something->agentid;
            $newpolicy -> activityid = $newvisit -> id;
                
                $newpolicy -> save();
                
            return response()->json(['success' => 'true', 'message' => 'Activity updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$something]);
        } 
    }
    
    public function referCustomer(\App\Http\Requests\UpdateActivityRequest $urequest)
    {        
        // for new & old customer
        $customer_id = $urequest->get('id','');
        $date_time_visit = $urequest->get('datetimevisit', '');
        $latitude = $urequest->get('lat','');
        $longitude = $urequest->get('lng','');
        $image_str = $urequest->get('imgstr', '');
        $remarks = $urequest->get('remarks', '');
        $numref = $urequest->get('numref', '');
        
        $newDate = \Carbon\Carbon::createFromFormat('d/m/Y', $date_time_visit)->format('Y-m-d');
        
        $activity_name = 'Referral';
        try
        {    
            if ($image_str != "")
            {
                $image = base64_decode(str_replace(' ', '+', $image_str));   
            
                //$img = imagecreatefromstring($image);

                $mytime = \Carbon\Carbon::now()->format('YmdHis');
                $test_path = '\img\visit'.$mytime.'.jpg';
                $image_path = public_path().$test_path;
                //Image::make($image->getRealPath())->save($image_path); 
                file_put_contents($image_path, $image);


                header('Content-Type: bitmap; charset=utf-8');
                $file = fopen($image_path, 'w');
                fwrite($file, $image);
                fclose($file);
            }
            else                
            {
                $test_path = '';
            }
            
            
            $newvisit = new \App\CustomerActivity();
            $newvisit -> customerid = $customer_id;
            $newvisit -> label = $activity_name;
            $newvisit -> activitydatetime = $newDate;
            $newvisit -> latitude = $latitude;
            $newvisit -> longitude = $longitude;
            $newvisit -> photo = $test_path;
            $newvisit -> remarks = $remarks;
            $newvisit -> numberrefer = $numref;
            $newvisit -> save();
            return response()->json(['success' => 'true', 'message' => 'Activity updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    public function recruitCustomer(\App\Http\Requests\UpdateActivityRequest $urequest)
    {        
        // for new & old customer
        $customer_id = $urequest->get('id','');
        $date_time_visit = $urequest->get('datetimevisit', '');
        $latitude = $urequest->get('lat','');
        $longitude = $urequest->get('lng','');
        $image_str = $urequest->get('imgstr', '');
        $remarks = $urequest->get('remarks', '');
        $recname = $urequest->get('recname', '');
        $recphone = $urequest->get('recphone', '');
        
        $newDate = \Carbon\Carbon::createFromFormat('d/m/Y', $date_time_visit)->format('Y-m-d');
        
        $activity_name = 'Referral';
        try
        {    
            if ($image_str != "")
            {
                $image = base64_decode(str_replace(' ', '+', $image_str));   
            
                //$img = imagecreatefromstring($image);

                $mytime = \Carbon\Carbon::now()->format('YmdHis');
                $test_path = '\img\visit'.$mytime.'.jpg';
                $image_path = public_path().$test_path;
                //Image::make($image->getRealPath())->save($image_path); 
                file_put_contents($image_path, $image);


                header('Content-Type: bitmap; charset=utf-8');
                $file = fopen($image_path, 'w');
                fwrite($file, $image);
                fclose($file);
            }
            else                
            {
                $test_path = '';
            }
            
            
            $newvisit = new \App\CustomerActivity();
            $newvisit -> customerid = $customer_id;
            $newvisit -> label = $activity_name;
            $newvisit -> activitydatetime = $newDate;
            $newvisit -> latitude = $latitude;
            $newvisit -> longitude = $longitude;
            $newvisit -> photo = $test_path;
            $newvisit -> remarks = $remarks;
            $newvisit -> recruit = $recname;
            $newvisit -> recruitphone = $recphone;
            $newvisit -> save();
            return response()->json(['success' => 'true', 'message' => 'Activity updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    public function view_customer($id)
    {        
        $something = \App\Customer::with('Agent')
            ->with('Policy')
            ->with('Activity')
            ->where('id', '=', $id)
            ->first();
        return \View('admin\view_customer', compact('something', $something));
    }
    
    public function uploadPhoto(Request $request)
    {
        
    }
    
    // </editor-fold>
}
