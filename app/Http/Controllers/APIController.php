<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class APIController extends Controller
{
    
    public function verify($username, $password)
    {
        //
        return \Auth::validate([
                    'email'    => $username,
                    'password' => $password,
                ]);
    }
    
    public function visitCustomer(Request $request)
    {
        // leave this empty then we know it is new customer
        $id = $request->get('id','');        
        $is_new_customer = $request->get('isnewcustomer','');
        
        // this is new customer only
        $customer_name = $request->get('customername', '');        
        $customer_phone_prefix = $request->get('customerphoneprefix', '');
        $customer_phone_number = $request->get('customerphone', '');
        $address = $request->get('address', '');
        
        // for new & old customer
        $customer_id = $request->get('customerid','');
        $date_time_visit = $request->get('datetimevisit', '');
        $latitude = $request->get('latitude', '');
        $longitude = $request->get('longitude', '');
        $image_str = $request->get('imagestr', '');
        
        $remarks = $request->get('remarks', '');
        
        //$fp = fopen($image_path ,'wb+');
        //fwrite($fp,$image);
        //fclose($fp);
        
        $activity_name = 'Visit';
        try
        {
            $image = base64_decode(str_replace(' ', '+', $image_str));   
            
            //$img = imagecreatefromstring($image);
        
            $mytime = \Carbon\Carbon::now()->format('YmdHis');
            $test_path = '\img\visit'.$mytime.'.jpg';
            $image_path = public_path().$test_path;
            //Image::make($image->getRealPath())->save($image_path); 
            file_put_contents($image_path, $image);
            
            
            header('Content-Type: bitmap; charset=utf-8');
            $file = fopen($image_path, 'w');
            fwrite($file, $image);
            fclose($file);

            if ($is_new_customer == "true")
            {
                $newcustomer = new \App\Customer(); 
                $newcustomer -> name = $customer_name;
                $newcustomer -> phoneprefix = $customer_phone_prefix;
                $newcustomer -> phone = $customer_phone_number;
                $newcustomer -> address = $address;
                $newcustomer -> agentid = $id;          
                $newcustomer -> updatedby = $id;
                $newcustomer -> save();
                
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $newcustomer -> id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $test_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> save();
                
                return response()->json(['status' => 'success', 'message' => $image_str]);
            }
            else
            {
                // old customer
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $customer_id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $test_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> save();
                return response()->json(['status' => 'success', 'message' => $image_str]);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => 'fail', 
                'message' => $ex->getMessage().$customer_name.$image_path]);
        } 
    }
    
    
    public function approachCustomer(Request $request)
    {
        // leave this empty then we know it is new customer
        $id = $request->get('id','');        
        $is_new_customer = $request->get('isnewcustomer','');
        
        // this is new customer only
        $customer_name = $request->get('customername', '');        
        $customer_phone_prefix = $request->get('customerphoneprefix', '');
        $customer_phone_number = $request->get('customerphone', '');
        $address = $request->get('address', '');
        
        // for new & old customer
        $customer_id = $request->get('customerid','');
        $date_time_visit = $request->get('datetimevisit', '');
        $latitude = $request->get('latitude', '');
        $longitude = $request->get('longitude', '');
        $potentialanp = $request->get('potentialanp', '');
        $image_str = $request->get('imagestr', '');
        $remarks = $request->get('remarks', '');
        
        $activity_name = 'Approach';
        try
        {
            $image = base64_decode(str_replace(' ', '+', $image_str));   
            
            //$img = imagecreatefromstring($image);
        
            $mytime = \Carbon\Carbon::now()->format('YmdHis');
            $test_path = '\img\visit'.$mytime.'.jpg';
            $image_path = public_path().$test_path;
            //Image::make($image->getRealPath())->save($image_path); 
            file_put_contents($image_path, $image);
            
            
            header('Content-Type: bitmap; charset=utf-8');
            $file = fopen($image_path, 'w');
            fwrite($file, $image);
            fclose($file);
            
            if ($is_new_customer == "true")
            {
                $newcustomer = new \App\Customer(); 
                $newcustomer -> name = $customer_name;
                $newcustomer -> phoneprefix = $customer_phone_prefix;
                $newcustomer -> phone = $customer_phone_number;
                $newcustomer -> agentid = $id;          
                $newcustomer -> updatedby = $id;
                $newcustomer -> address = $address;
                $newcustomer -> save();
                
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $newcustomer -> id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $test_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> potentialanp = $potentialanp;
                $newvisit -> save();
                
                return response()->json(['status' => 'success']);
            }
            else
            {
                // old customer
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $customer_id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $test_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> potentialanp = $potentialanp;
                $newvisit -> save();
                return response()->json(['status' => 'success']);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => 'fail', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    
    public function dealCustomer(Request $request)
    {
        // leave this empty then we know it is new customer
        $id = $request->get('id','');        
        $is_new_customer = $request->get('isnewcustomer','');
        
        // this is new customer only
        $customer_name = $request->get('customername', '');        
        $customer_phone_prefix = $request->get('customerphoneprefix', '');
        $customer_phone_number = $request->get('customerphone', '');
        $address = $request->get('address', '');
        
        // for new & old customer
        $customer_id = $request->get('customerid','');
        $date_time_visit = $request->get('datetimevisit', '');
        $latitude = $request->get('latitude', '');
        $longitude = $request->get('longitude', '');
        $image_str = $request->get('imagestr', '');
        $remarks = $request->get('remarks', '');
        
        // more things - policy
        $startdate = $request->get('policystartdate', '');
        $paymentterm = $request->get('term', '');
        $paymentmode = $request->get('mode', '');
        $insuredname = $request->get('insuredname', '');
        $anp = $request->get('anp', '');
        $fyp = $request->get('fyp', '');
        
        $activity_name = 'Deal';
        try
        {
            $image = base64_decode(str_replace(' ', '+', $image_str));   
            
            //$img = imagecreatefromstring($image);
        
            $mytime = \Carbon\Carbon::now()->format('YmdHis');
            $test_path = '\img\visit'.$mytime.'.jpg';
            $image_path = public_path().$test_path;
            //Image::make($image->getRealPath())->save($image_path); 
            file_put_contents($image_path, $image);
            
            
            header('Content-Type: bitmap; charset=utf-8');
            $file = fopen($image_path, 'w');
            fwrite($file, $image);
            fclose($file);
            
            if ($is_new_customer == "true")
            {
                $newcustomer = new \App\Customer(); 
                $newcustomer -> name = $customer_name;
                $newcustomer -> phoneprefix = $customer_phone_prefix;
                $newcustomer -> phone = $customer_phone_number;
                $newcustomer -> agentid = $id;          
                $newcustomer -> updatedby = $id;
                $newcustomer -> address = $address;
                $newcustomer -> save();
                
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $newcustomer -> id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $test_path;
                $newvisit -> remarks = $remarks;
                
                $newvisit -> save();
                
                $newpolicy = new \App\Policy();
                $newpolicy -> customer = $newcustomer -> id;
                $newpolicy -> agent = $id;
                $newpolicy -> policyremark = $remarks;
                $newpolicy -> startdate = $startdate;
                $newpolicy -> policyterm = $paymentterm;
                $newpolicy -> policymode = $paymentmode;
                $newpolicy -> insuredname = $insuredname;
                $newpolicy -> anp = $anp;
                $newpolicy -> updatedby = $id;
                $newpolicy -> fyp = $fyp;
                
                $newpolicy -> save();
                
                return response()->json(['status' => 'success']);
            }
            else
            {
                // old customer
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $customer_id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $test_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> save();
                
                $newpolicy = new \App\Policy();
                $newpolicy -> customer = $customer_id;
                $newpolicy -> agent = $id;
                $newpolicy -> policyremark = $remarks;
                $newpolicy -> startdate = $startdate;
                $newpolicy -> insuredname = $insuredname;
                $newpolicy -> policyterm = $paymentterm;
                $newpolicy -> policymode = $paymentmode;
                $newpolicy -> anp = $anp;
                $newpolicy -> fyp = $fyp;
                $newpolicy -> updatedby = $id;
                
                $newpolicy -> save();
                
                return response()->json(['status' => 'success']);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => 'fail', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    
    public function referCustomer(Request $request)
    {
        // leave this empty then we know it is new customer
        $id = $request->get('id','');        
        $is_new_customer = $request->get('isnewcustomer','');
        
        // this is new customer only
        $customer_name = $request->get('customername', '');        
        $customer_phone_prefix = $request->get('customerphoneprefix', '');
        $customer_phone_number = $request->get('customerphone', '');
        $address = $request->get('address', '');
        
        // for new & old customer
        $customer_id = $request->get('customerid','');
        $date_time_visit = $request->get('datetimevisit', '');
        $latitude = $request->get('latitude', '');
        $longitude = $request->get('longitude', '');
        $image_str = $request->get('imagestr', '');
        $remarks = $request->get('remarks', '');
        
        // more things - policy
        $numref = $request->get('numref', '');
        
        $activity_name = 'Referral';
        try
        {
            $image = base64_decode(str_replace(' ', '+', $image_str));   
            
            //$img = imagecreatefromstring($image);
        
            $mytime = \Carbon\Carbon::now()->format('YmdHis');
            $test_path = '\img\visit'.$mytime.'.jpg';
            $image_path = public_path().$test_path;
            //Image::make($image->getRealPath())->save($image_path); 
            file_put_contents($image_path, $image);
            
            header('Content-Type: bitmap; charset=utf-8');
            $file = fopen($image_path, 'w');
            fwrite($file, $image);
            fclose($file);
            
            if ($is_new_customer == "true")
            {
                $newcustomer = new \App\Customer(); 
                $newcustomer -> name = $customer_name;
                $newcustomer -> phoneprefix = $customer_phone_prefix;
                $newcustomer -> phone = $customer_phone_number;
                $newcustomer -> agentid = $id;          
                $newcustomer -> updatedby = $id;
                $newcustomer -> address = $address;
                $newcustomer -> save();
                
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $newcustomer -> id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $image_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> numberrefer = $numref;
                
                $newvisit -> save();
                
                
                
                return response()->json(['status' => 'success']);
            }
            else
            {
                // old customer
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $customer_id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $image_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> numberrefer = $numref;
                $newvisit -> save();
                
                
                return response()->json(['status' => 'success']);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => 'fail', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    public function recruitCustomer(Request $request)
    {
        // leave this empty then we know it is new customer
        $id = $request->get('id','');        
        $is_new_customer = $request->get('isnewcustomer','');
        
        // this is new customer only
        $customer_name = $request->get('customername', '');        
        $customer_phone_prefix = $request->get('customerphoneprefix', '');
        $customer_phone_number = $request->get('customerphone', '');
        $address = $request->get('address', '');
        
        // for new & old customer
        $customer_id = $request->get('customerid','');
        $date_time_visit = $request->get('datetimevisit', '');
        $latitude = $request->get('latitude', '');
        $longitude = $request->get('longitude', '');
        $image_str = $request->get('imagestr', '');
        $remarks = $request->get('remarks', '');
        
        // more things - policy
        $recname = $request->get('recname', '');
        $recphone = $request->get('recphone', '');
        
        $activity_name = 'Recruit';
        try
        {
            $image = base64_decode(str_replace(' ', '+', $image_str));   
            
            //$img = imagecreatefromstring($image);
        
            $mytime = \Carbon\Carbon::now()->format('YmdHis');
            $test_path = '\img\visit'.$mytime.'.jpg';
            $image_path = public_path().$test_path;
            //Image::make($image->getRealPath())->save($image_path); 
            file_put_contents($image_path, $image);
            
            header('Content-Type: bitmap; charset=utf-8');
            $file = fopen($image_path, 'w');
            fwrite($file, $image);
            fclose($file);
            
            if ($is_new_customer == "true")
            {
                $newcustomer = new \App\Customer(); 
                $newcustomer -> name = $customer_name;
                $newcustomer -> phoneprefix = $customer_phone_prefix;
                $newcustomer -> phone = $customer_phone_number;
                $newcustomer -> agentid = $id;          
                $newcustomer -> updatedby = $id;
                $newcustomer -> address = $address;
                $newcustomer -> save();
                
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $newcustomer -> id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $image_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> recruit = $recname;
                $newvisit -> recruitphone = $recphone;
                
                $newvisit -> save();
                
                
                
                return response()->json(['status' => 'success']);
            }
            else
            {
                // old customer
                $newvisit = new \App\CustomerActivity();
                $newvisit -> customerid = $customer_id;
                $newvisit -> label = $activity_name;
                $newvisit -> activitydatetime = $date_time_visit;
                $newvisit -> latitude = $latitude;
                $newvisit -> longitude = $longitude;
                $newvisit -> photo = $image_path;
                $newvisit -> remarks = $remarks;
                $newvisit -> recruit = $recname;
                $newvisit -> recruitphone = $recphone;
                $newvisit -> save();
                
                
                return response()->json(['status' => 'success']);
            }
        } catch (\Exception $ex) {
            return response()->json(['status' => 'fail', 
                'message' => $ex->getMessage().$customer_name]);
        } 
    }
    
    public function retrieveCustomers(Request $request)
    {
        $name = $request -> get('name','');
        $agent = $request -> get('agentid', '');
        $phonenumber = $request -> get('phonenumber', '');
        
        if ($agent == '')
        {
            return response()->json(['status' => 'fail', 
                'message' => 'agent id is compulsory']);
        }
        
        if($name == ''
                && $agent == ''
                && $phonenumber == '')
        {            // all empty get all
            $something = \App\Customer::with('Agent') ->with('Policy') ->get();
        }
        else
        {
            $something = \App\Customer::
                with('Agent') ->
                with('Policy') ->
                whereHas('Agent', function($query) use($agent) {
                    $query->where('id', '=', $agent);
                })->
                where('name', 'LIKE', '%'.$name.'%')
                    ->where(\DB::raw('CONCAT_WS(" ", phoneprefix, phone)'), 'like', '%'.$phonenumber.'%')                    
                    ->get();
        }
        return response()->json(['success' => true, 'members' => $something]);        
        
    }
    
    public function view_customer($id)
    {        
        $something = \App\Customer::with('Agent')
            ->with('Policy')
            ->with('Activity')
            ->where('id', '=', $id)
            ->first();
        return response()->json(['success' => true, 'customer' => $something]);
    }
    
    public function retrieveProfile($id)
    {
        $something = \DB::table('users as u1')
            ->rightJoin('users as u2', 'u2.uplineid', '=', 'u1.id')
            ->where('u2.email', '=', $id)
            ->select('u2.*', 'u1.name as uplinename')
            ->first();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    public function changePassword(Request $request)
    {
        $oldpassword = $request->get('oldpassword','');
        $newpassword = $request->get('newpassword','');
        $id = $request->get('id','');
        
        $newMember = \App\User::find($id);
        
        if (\Hash::check($oldpassword, $newMember -> password))
        {
            $newMember -> password = bcrypt($newpassword); 
            $newMember -> save();
            return response()->json(['status' => 'success', 
                'member' => '',
                'message' => 'password changed successfully']);
        }
        else
        {
            return response()->json(['status' => 'fail', 
                'member' => '',
                'message' => 'invalid old password']);
        }
        
    }
    
    public function retrieveActivityList(Request $request)
    {
        $customerid = $request -> get('customerid', '');  
        $label = $request -> get('label', '');
        
        if ($customerid == '')
        {
            return response()->json(['status' => 'fail', 
                'message' => 'customer id is compulsory']);
        }
        
        if ($label == '')
        {
            $something = \App\CustomerActivity::
                where('customerid', '=', $customerid)
                    ->get();
        }
        else {
            $something = \App\CustomerActivity::
                where('customerid', '=', $customerid) ->
                where('label', '=', $label) ->
                orderBy('activitydatetime', 'desc')                    
                ->get();
        }        
        
        return response()->json(['success' => true, 'activities' => $something]); 
    }
    
    public function retriveAllActivityList(Request $request)
    {
        $agent = $request -> get('agentid', '');
        
        if ($agent == '')
        {
            return response()->json(['status' => 'fail', 
                'message' => 'agent id is compulsory']);
        }
        
        $something = \App\CustomerActivity::
                with('customers') ->
                whereHas('customers', function($query) use($agent) {
                    $query->where('agentid', '=', $agent);
                })->
                orderBy('activitydatetime', 'desc')
                ->get();  
        
        return response()->json(['success' => true, 'members' => $something]);  
    }
    
    public function get_policybyactivity($id)
    {
        $something = \App\Policy::
            where('activityid', '=', $id)
            ->first();
        return response()->json(['success' => true, 'member' => $something]);
    }
    
    
    public function update_policy(\App\Http\Requests\UpdatePolicyRequest $urequest)
    {        
        // for new & old customer
        $id = $urequest->get('id', '');
        $policyid = $urequest->get('policyid', '');
        $anp = $urequest->get('anp', '');
        $fyp = $urequest->get('fyp', '');
        $insuredname = $urequest->get('insuredname', '');
        $paymentmode = $urequest->get('paymentmode', '');
        $paymentfrequency = $urequest->get('paymentfrequency', '');
        $policystart = $urequest->get('policystart', '');
        
        try
        {    
            $newpolicy = \App\Policy::find($id);
            $newpolicy -> policyid = $policyid;
            $newpolicy -> startdate = $policystart;
            $newpolicy -> policyterm = $paymentfrequency;
            $newpolicy -> policymode = $paymentmode;
            $newpolicy -> insuredname = $insuredname;
            $newpolicy -> anp = $anp;
            $newpolicy -> fyp = $fyp;
            $newpolicy -> save();
                
            return response()->json(['success' => 'true', 'message' => 'Policy updated successfully.']);
            
        } catch (\Exception $ex) {
            return response()->json(['success' => 'false', 
                'message' => $ex->getMessage().$something]);
        } 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
