<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerActivity extends Model
{
    //
    protected $table = 'customer_activities';
    
    public function customers()
    {
        return $this->belongsTo('App\Customer', 'customerid', 'id');
    }
}
