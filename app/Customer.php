<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'customers';
    
    public function Policy()
    {
        return $this->hasMany('App\Policy','customer','id');
    }
    
    public function Agent()
    {
        return $this->belongsTo('App\User', 'agentid', 'id');
    }
    
    public function Activity()
    {
        return $this->hasMany('App\CustomerActivity','customerid','id');
    }
}
